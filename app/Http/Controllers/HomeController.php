<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\travel;
use App\travelimage;
use App\car;
use App\carimage;
use App\blog;
use App\cat;
use App\image;
use App\mobile;
use App\mobileimage;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function tour_and_travel()
    {
        $travel=DB::table('travels')->get();
        return view('tour-and-travel')->with('travel',$travel);
    }

    public function tour_and_travel_details()
    {
        $id=$_GET['id'];
        $travel=DB::table('travels')->where('id',$id)->get();
        return view('tour-and-travel-details')->with('travel',$travel);
    }
    
    
    public function menu()
    {
        return view('menu');
    }

    
    public function cab_services()
    {
        return view('cab-services');
    }


    public function cab_services_details()
    {
        $id=$_GET['id'];
        $car=DB::table('cars')->where('id',$id)->get();

        return view('cab-services-details')->with('car',$car);
    }

    public function news()
    {
         
        $cat=DB::table('cats')->get();

        return view('news')->with('cat',$cat);  
    }

    
    public function news_details()
    {
         $id=$_GET['id'];
        $cat=DB::table('cats')->where('id',$id)->get();

        return view('news-details')->with('cat',$cat);  
    }

    public function contact()
    {
        return view('contact');
    }
    
    public function about_us()
    {
        return view('about-us');
    }
    
    public function booking()
    {
        return view('booking');
    }





    
    public function myadmin()
    {
        return view('admin/dashboard');
    }

    
    public function tour_travel_add()
    {
        return view('admin/tour-travel-add');
    }

    //Add_Tour_Travel
    public function add_trvel(Request $ress)
    {
        $travel=new travel;
        $travel->title= $ress->input('title');
        $travel->price= $ress->input('price');
        $travel->description = $ress->input('description');
        $travel->remember_token = $ress->input('_token');


        $travel->save();
        
        

        $image_array=$ress->file('image');
        $id=$travel->id;
        for($i=0;$i<count($image_array);$i++)
{
               

                
               
                $my_image=new travelimage;
                $my_image->travel_id=$id;
                $image_ext=$image_array[$i]->getClientOriginalExtension();
                $new_image_name=rand(123456,999999).".".$image_ext;
                $destination_path=public_path("/upload/");
                $image_array[$i]->move($destination_path,$new_image_name);
                $my_image->image=$new_image_name;
                $my_image->remember_token=$ress->input('_token');
                $my_image->save();

                
            }
        
       
        
        
        $travel=DB::table('travels')->get();

        return view('admin/tour-travel')->with('travel',$travel);  
    }


    //View_Tour_Travel
    
    public function tour_travel()
    {
        $travel=DB::table('travels')->get();

        return view('admin/tour-travel')->with('travel',$travel); 
    }

    //Delete_Tour_Travel
    
    public function del_travel()
    {
        $id=$_GET['id'];
        DB::table ('travels')->where('id',$id)->delete();
        $travel=DB::table('travels')->get();

        return view('admin/tour-travel')->with('travel',$travel); 
    }

    

    public function cab_service_add()
    {
        return view('admin/cab-service-add');
    }

    //Add_Cab_Servie
    public function add_cab(Request $ress)
    {
        $cab=new car;
        $cab->title= $ress->input('title');
        $cab->subtitle= $ress->input('subtitle');
        $cab->price= $ress->input('price');
        $cab->guest= $ress->input('guest');
        $cab->description = $ress->input('description');
        $cab->remember_token = $ress->input('_token');
        $cab->save();

        $image_array=$ress->file('image');
        $id=$cab->id;
        for($i=0;$i<count($image_array);$i++)
{
                $my_image=new carimage;
                $my_image->car_id=$id;
                $image_ext=$image_array[$i]->getClientOriginalExtension();
                $new_image_name=rand(123456,999999).".".$image_ext;
                $destination_path=public_path("/upload/");
                $image_array[$i]->move($destination_path,$new_image_name);
                $my_image->image=$new_image_name;
                $my_image->remember_token=$ress->input('_token');
                $my_image->save();

            }
        
        $cab=DB::table('cars')->get();

        return view('admin/cab-service')->with('cab',$cab);  
    }

    //View_Cab_Service

    public function cab_service()
    {
        $cab=DB::table('cars')->get();

        return view('admin/cab-service')->with('cab',$cab);  
    }

    //Delete_cab_Service
   
    public function  del_cab()
    {
        $id=$_GET['id'];
        DB::table ('cars')->where('id',$id)->delete();
        $cab=DB::table('cars')->get();

        return view('admin/cab-service')->with('cab',$cab);  
    }

  
     
    public function file_upload()
    {
        return view('admin/file_upload');
    }


    public function blog_add()
    {
        return view('admin/blog-add');
    }

    //Add_Blogs 
    public function add_blogs(Request $ress)
    {
        $blog=new blog;
        $blog->title= $ress->input('title');
        $blog->subtitle= $ress->input('subtitle');
        $blog->description = $ress->input('description');
       
        
        $image_array=$ress->file('image');
        $image_ext=$image_array->getClientOriginalExtension();
        $new_image_name=rand(123456,999999).".".$image_ext;
        $destination_path=public_path("/upload/");
        $image_array->move($destination_path,$new_image_name);
        $blog->image=$new_image_name;
        
        $blog->remember_token = $ress->input('_token');


        $blog->save();

        
        $blog=DB::table('blogs')->get();

        return view('admin/blog-all')->with('blog',$blog);  
    }


    //View_Blogs
    public function blog_all()
    {
        $blog=DB::table('blogs')->get();

        return view('admin/blog-all')->with('blog',$blog); 
    }

    //Delete_Blog 
    public function del_blog()
    {
        $id=$_GET['id'];
        DB::table ('blogs')->where('id',$id)->delete();
        $blog=DB::table('blogs')->get();

        return view('admin/blog-all')->with('blog',$blog);  
    }

    
    public function news_add()
    {
        return view('admin/news-add');
    }

    //Add_News 
    public function add_news(Request $ress)
    {
        $cat=new cat;
        $cat->title= $ress->input('title');
        $cat->subtitle= $ress->input('subtitle');
        $cat->description = $ress->input('description');
       
        
        $image_array=$ress->file('image');
        $image_ext=$image_array->getClientOriginalExtension();
        $new_image_name=rand(123456,999999).".".$image_ext;
        $destination_path=public_path("/upload/");
        $image_array->move($destination_path,$new_image_name);
        $cat->image=$new_image_name;
        
        $cat->remember_token = $ress->input('_token');


        $cat->save();

        
        $cat=DB::table('cats')->get();

        return view('admin/news-all')->with('cat',$cat);  
    }

    //View_News 
    
    public function news_all()
    {
        $cat=DB::table('cats')->get();

        return view('admin/news-all')->with('cat',$cat); 
    }

    //Delete_News
    public function del_news()
    {
        $id=$_GET['id'];
        DB::table ('cats')->where('id',$id)->delete();
        $cat=DB::table('cats')->get();

        return view('admin/news-all')->with('cat',$cat); 
    }

    

    public function gallery_add()
    {
        return view('admin/gallery-add');
    }

    //Add_gallery
    public function add_gal(Request $ress)
    {
        $image=new image;
        $image->title= $ress->input('title');
       
       
        
        $image_array=$ress->file('image');
        $image_ext=$image_array->getClientOriginalExtension();
        $new_image_name=rand(123456,999999).".".$image_ext;
        $destination_path=public_path("/upload/");
        $image_array->move($destination_path,$new_image_name);
        $image->image=$new_image_name;
        
        $image->remember_token = $ress->input('_token');


        $image->save();

        
        $image=DB::table('images')->get();

        return view('admin/gallery-all')->with('image',$image);  
    }

    //View_gallery
    
    public function gallery_all()
    {
        $image=DB::table('images')->get();

        return view('admin/gallery-all')->with('image',$image);  
    }

    //Delete_Gallery 
    public function del_gal()
    {
        $id=$_GET['id'];
        DB::table ('images')->where('id',$id)->delete();
        $image=DB::table('images')->get();

        return view('admin/gallery-all')->with('image',$image);  
    }
    


    
    public function mobile_accessories_add()
    {
        return view('admin/mobile-accessories-add');
    }

    //Add_Mobile_Accessories
     public function add_mob(Request $ress)
     {
        $mobile=new mobile;
        $mobile->title= $ress->input('title');
        $mobile->description= $ress->input('description');
        $mobile->price= $ress->input('price');
       
        $mobile->remember_token = $ress->input('_token');
        $mobile->save();

        $image_array=$ress->file('image');
        $id=$mobile->id;
        for($i=0;$i<count($image_array);$i++)
{
                $my_image=new mobileimage;
                $my_image->mobile_id=$id;
                $image_ext=$image_array[$i]->getClientOriginalExtension();
                $new_image_name=rand(123456,999999).".".$image_ext;
                $destination_path=public_path("/upload/");
                $image_array[$i]->move($destination_path,$new_image_name);
                $my_image->image=$new_image_name;
                $my_image->remember_token=$ress->input('_token');
                $my_image->save();

            }
        
        $mobile=DB::table('mobiles')->get();

        return view('admin/mobile-accessories')->with('mobile',$mobile);  
     }

    //View_Mobile_accessories
        
    public function mobile_accessories()
    {
        $mobile=DB::table('mobiles')->get();

        return view('admin/mobile-accessories')->with('mobile',$mobile);  
    }

    //Delete_Mobile_accessories
    
    public function del_mobile()
    {
        $id=$_GET['id'];
        DB::table ('mobiles')->where('id',$id)->delete();
        $mobile=DB::table('mobiles')->get();

        return view('admin/mobile-accessories')->with('mobile',$mobile);  
    }
    
}
