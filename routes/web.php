<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/tour-and-travel', 'HomeController@tour_and_travel');
Route::get('/tour-and-travel-details', 'HomeController@tour_and_travel_details');
Route::get('/menu', 'HomeController@menu');
Route::get('/cab-services', 'HomeController@cab_services');
Route::get('/cab-services-details', 'HomeController@cab_services_details');
Route::get('/news', 'HomeController@news');
Route::get('/news-details', 'HomeController@news_details');
Route::get('/contact', 'HomeController@contact');
Route::get('/about-us', 'HomeController@about_us');
Route::get('/booking', 'HomeController@booking');


Route::get('/myadmin', 'HomeController@myadmin');
Route::get('/file_upload', 'HomeController@file_upload');
Route::get('/tour-travel-add', 'HomeController@tour_travel_add');
Route::get('/cab-service-add', 'HomeController@cab_service_add');
Route::get('/blog-add', 'HomeController@blog_add');
Route::get('/news-add', 'HomeController@news_add');
Route::get('/gallery-add', 'HomeController@gallery_add');
Route::get('/mobile-accessories-add', 'HomeController@mobile_accessories_add');

//Add_Tour_Travel
Route::post('/add_trvel', 'HomeController@add_trvel');

//Add_Cab_Service
Route::post('/add_cab', 'HomeController@add_cab');

//Add_Blog
Route::post('/add_blogs', 'HomeController@add_blogs');

//Add_News
Route::post('/add_news', 'HomeController@add_news');

//Add_gallery
Route::post('/add_gal', 'HomeController@add_gal');

//Add_mobile_accessories
Route::post('/add_mob', 'HomeController@add_mob');




//View_Tour_Travel
Route::get('/tour-travel', 'HomeController@tour_travel');

//View_Cab_Service
Route::get('/cab-service', 'HomeController@cab_service');

//View_Blog
Route::get('/blog-all', 'HomeController@blog_all');

//View_News
Route::get('/news-all', 'HomeController@news_all');

//View_Gallery
Route::get('/gallery-all', 'HomeController@gallery_all');

//View_mobile-accessories
Route::get('/mobile-accessories', 'HomeController@mobile_accessories');





//Delete_Tour_Travel
Route::get('/del_travel', 'HomeController@del_travel');

//Delete_cab_Service
Route::get('/del_cab', 'HomeController@del_cab');

//Delete_Blog
Route::get('/del_blog', 'HomeController@del_blog');

//Delete_News
Route::get('/del_news', 'HomeController@del_news');

//Delete_Gallery
Route::get('/del_gal', 'HomeController@del_gal');

//Delete_Mobile_accessories
Route::get('/del_mobile', 'HomeController@del_mobile');
