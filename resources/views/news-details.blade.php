@extends('layouts.master')
@section('body')

@foreach($cat as $cat)
<div class="bravo-news">
<div class="bravo_banner" style="background-image: url(../../uploads/demo/news/news-banner.jpg)">
<div class="container">
<h1>
News
</h1>
</div>
</div>
<div class="blog-breadcrumb hidden-xs">
<div class="container">
<ul>
<li><a href="#"> Home</a></li>
<li class="">
<a href="#">News</a>
</li>
<li class="active">
All Aboard the Rocky Mountaineer
 </li>
</ul>
</div>
</div>
<div class="bravo_content">
<div class="container">
<div class="row">
<div class="col-md-9">
<div class="article">
<div class="header">
<header class="post-header">
<img src="upload/{{$cat->image}}" alt="All Aboard the Rocky Mountaineer">
</header>
<div class="cate">
<ul>
<li>
<a href="category\hosted-tour.html">
    {{$cat->title}}
</a>
</li>
</ul>
</div>
</div>
<h2 class="title">{{$cat->subtitle}}</h2>
<div class="post-info">
<ul>
<li>

<li>{{$cat->created_at}}</li>
</ul>
</div>
<div class="post-content">{{$cat->description}}</div>

<div class="space-between">
<div class="share"> Share
<a class="facebook share-item" href="https://www.facebook.com/sharer/sharer.php?u=https://bookingcore.org/en/news/all-aboard-the-rocky-mountaineer&title=All Aboard the Rocky Mountaineer" target="_blank" original-title="Facebook"><i class="fa fa-facebook fa-lg"></i></a>
<a class="twitter share-item" href="https://twitter.com/share?url=https://bookingcore.org/en/news/all-aboard-the-rocky-mountaineer&title=All Aboard the Rocky Mountaineer" target="_blank" original-title="Twitter"><i class="fa fa-twitter fa-lg"></i></a>
</div>
</div>
</div>
</div>
<div class="col-md-3">
<aside class="sidebar-right">
<div class="sidebar-widget widget_search">
<form method="get" class="search" action="https://bookingcore.org/en/news">
<input type="text" class="form-control" value="" name="s" placeholder="Search ...">
<button type="submit" class="icon_search"></button>
</form>
</div> <div class="sidebar-widget">
<div class="sidebar-title">
<h4>About Us</h4>
</div>
<div class="textwidget">
Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus, quis mollis nisl nunc et massa
</div>
</div> <div class="sidebar-widget widget_bloglist">
<div class="sidebar-title">
<h4>Recent News</h4>
</div>
<ul class="thumb-list">

    @php($cat=DB::table('cats')->get())
    @foreach($cat as $cat)
<li>
<div class="thumb">
<a href="/news-details?id={{$cat->id}}">
<img class=' lazy' data-src="upload/{{$cat->image}}" alt='Morning in the Northern sea'>
</a>
</div>
<div class="content">
<div class="cate">
<a href="/news-details?id={{$cat->id}}">
    {{$cat->title}}
</a>
</div>
<h5 class="thumb-list-item-title">
<a href="/news-details?id={{$cat->id}}">{{$cat->subtitle}}</a>
</h5>
</div>
</li>
@endforeach



</ul>
</div>

<div class="sidebar-widget widget_tag_cloud">

<div class="tagcloud">
<ul>
</ul>
</div>
</div>
</aside> </div>
</div>
</div>
</div>
</div>

@endforeach
@endsection