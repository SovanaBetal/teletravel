<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html class="supernova"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="alternate" type="application/json+oembed" href="https://www.jotform.com/oembed/?format=json&url=https%3A%2F%2Fform.jotform.com%2F200221584070038" title="oEmbed Form">
<link rel="alternate" type="text/xml+oembed" href="https://www.jotform.com/oembed/?format=xml&url=https%3A%2F%2Fform.jotform.com%2F200221584070038" title="oEmbed Form">
<meta property="og:title" content="Sunset Hair">
<meta property="og:url" content="https://form.jotform.com/200221584070038">
<meta property="og:description" content="Please click the link to complete this form.">
<meta name="slack-app-id" content="AHNMASS8M">
<link rel="shortcut icon" href="https://cdn.jotfor.ms/favicon.ico">
<link rel="canonical" href="200221584070038.html">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=1">
<meta name="HandheldFriendly" content="true">
<title>Sunset Hair</title>
<link href="https://cdn.jotfor.ms/static/formCss.css?3.3.15016" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/css/styles/nova.css?3.3.15016">
<link type="text/css" media="print" rel="stylesheet" href="https://cdn.jotfor.ms/css/printForm.css?3.3.15016">
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/themes/CSS/5495488a700cc478508b4567.css?themeRevisionID=5cf3a016dd9c5671876e9d31">
<style type="text/css">
@import url('https://shots.jotform.com/elton/genericTheme.css');n@import url(https://fonts.googleapis.com/css?family=Abel);

    .form-label-left{
        width:150px;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px;
    }
    body, html{
        margin:0;
        padding:0;
        background:rgba(39, 38, 38, 0.56);
    }

    .form-all{
        margin:0px auto;
        padding-top:0px;
        width:580px;
        color:rgb(255, 255, 255) !important;
        font-family:'Roboto';
        font-size:18px;
    }
    .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
        color: rgb(0, 0, 0);
    }

</style>

<style type="text/css" id="form-designer-style">
    /* Injected CSS Code */
.form-label.form-label-auto {
        
        display: inline-block;
        float: left;
        text-align: left;
      
      }/*PREFERENCES STYLE*/
    .form-all {
      font-family: Roboto, sans-serif;
    }
    .form-all .qq-upload-button,
    .form-all .form-submit-button,
    .form-all .form-submit-reset,
    .form-all .form-submit-print {
      font-family: Roboto, sans-serif;
    }
    .form-all .form-pagebreak-back-container,
    .form-all .form-pagebreak-next-container {
      font-family: Roboto, sans-serif;
    }
    .form-header-group {
      font-family: Roboto, sans-serif;
    }
    .form-label {
      font-family: Roboto, sans-serif;
    }
  
    
  
    .form-line {
      margin-top: 12px;
      margin-bottom: 12px;
    }
  
    .form-all {
      width: 580px;
    }
  
    .form-label-left,
    .form-label-right,
    .form-label-left.form-label-auto,
    .form-label-right.form-label-auto {
      width: 150px;
    }
  
    .form-all {
      font-size: 18px
    }
    .form-all .qq-upload-button,
    .form-all .qq-upload-button,
    .form-all .form-submit-button,
    .form-all .form-submit-reset,
    .form-all .form-submit-print {
      font-size: 18px
    }
    .form-all .form-pagebreak-back-container,
    .form-all .form-pagebreak-next-container {
      font-size: 18px
    }
  
    .supernova .form-all, .form-all {
      background-color: rgba(39, 38, 38, 0.56);
      border: 1px solid transparent;
    }
  
    .form-all {
      color: rgb(255, 255, 255);
    }
    .form-header-group .form-header {
      color: rgb(255, 255, 255);
    }
    .form-header-group .form-subHeader {
      color: rgb(255, 255, 255);
    }
    .form-label-top,
    .form-label-left,
    .form-label-right,
    .form-html,
    .form-checkbox-item label,
    .form-radio-item label {
      color: rgb(255, 255, 255);
    }
    .form-sub-label {
      color: 1a1a1a;
    }
  
    .supernova {
      background-color: rgb(255, 255, 255);
    }
    .supernova body {
      background: transparent;
    }
  
    .form-textbox,
    .form-textarea,
    .form-radio-other-input,
    .form-checkbox-other-input,
    .form-captcha input,
    .form-spinner input {
      background-color: rgba(255, 255, 255, 0.83);
    }
  
      .supernova {
        height: 100%;
        background-repeat: repeat;
        background-attachment: scroll;
        background-position: center top;
      }
      .supernova {
        background-image: url("https://www.jotform.com/uploads/guest_200221645733042/form_files/background1.5e2972346e8f83.16210333.jpg");
      }
      #stage {
        background-image: url("https://www.jotform.com/uploads/guest_200221645733042/form_files/background1.5e2972346e8f83.16210333.jpg");
      }
    
    .form-all {
      background-image: none;
    }
  
  .ie-8 .form-all:before { display: none; }
  .ie-8 {
    margin-top: auto;
    margin-top: initial;
  }
  
  /*PREFERENCES STYLE*//*__INSPECT_SEPERATOR__*/
    /* Injected CSS Code */
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/punycode/1.4.1/punycode.min.js"></script>
<script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.15016" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/js/vendor/math-processor.js?v=3.3.15016" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.setCalculations([{"decimalPlaces":"2","equation":"#input_3[*0.12]","ignoreHiddenFields":"","insertAsText":"","newCalculationType":"1","operands":"","readOnly":"1","resultField":"14","showBeforeInput":"","showEmptyDecimals":""}]);
	JotForm.init(function(){
if (window.JotForm && JotForm.accessible) $(input_18).setAttribute('tabindex',0);
if (window.JotForm && JotForm.accessible) $(input_33).setAttribute('tabindex',0);

 JotForm.calendarMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
 JotForm.calendarDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
 JotForm.calendarOther = {"today":"Today"};
 var languageOptions = document.querySelectorAll('#langList li'); 
 for(var langIndex = 0; langIndex < languageOptions.length; langIndex++) { 
   languageOptions[langIndex].on('click', function(e) { setTimeout(function(){ JotForm.setCalendar("30", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":true,"custom":false,"ranges":false,"start":"","end":""}); }, 0); });
 } 
 JotForm.setCalendar("30", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":true,"custom":false,"ranges":false,"start":"","end":""});

 JotForm.calendarMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
 JotForm.calendarDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
 JotForm.calendarOther = {"today":"Today"};
 var languageOptions = document.querySelectorAll('#langList li'); 
 for(var langIndex = 0; langIndex < languageOptions.length; langIndex++) { 
   languageOptions[langIndex].on('click', function(e) { setTimeout(function(){ JotForm.setCalendar("31", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":true,"custom":false,"ranges":false,"start":"","end":""}); }, 0); });
 } 
 JotForm.setCalendar("31", false, {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},"future":true,"past":true,"custom":false,"ranges":false,"start":"","end":""});
    if(typeof $('input_34').spinner !== 'undefined') {$('input_34').spinner({ imgPath:'https://cdn.jotfor.ms/images/', width: '60', maxValue:'', minValue:'', allowNegative: false, addAmount: 1, value:'' });}
    $('input_34').up(2).setAttribute('tabindex',0);
	JotForm.clearFieldOnHide="disable";
    /*INIT-END*/
	});

   JotForm.prepareCalculationsOnTheFly([null,{"name":"enquiryForm","qid":"1","text":"Enquiry Form","type":"control_head"},{"name":"submit","qid":"2","text":"Submit","type":"control_button"},null,null,null,null,null,null,null,null,null,null,null,null,{"name":"email15","qid":"15","subLabel":"","text":"E-mail","type":"control_email"},{"name":"contactNumber","qid":"16","text":"Contact Number","type":"control_phone"},null,{"name":"name","qid":"18","text":"Name","type":"control_textbox"},{"name":"address19","qid":"19","text":"Address","type":"control_address"},null,null,null,null,null,null,null,null,null,null,{"description":"","name":"date","qid":"30","text":"Start Date","type":"control_datetime"},{"description":"","name":"date31","qid":"31","text":"End Date","type":"control_datetime"},{"name":"divider","qid":"32","type":"control_divider"},{"description":"","name":"typeA","qid":"33","subLabel":"","text":"Service For","type":"control_textbox"},{"description":"","name":"typeA34","qid":"34","subLabel":"","text":"Guest ","type":"control_spinner"}]);
   setTimeout(function() {
JotForm.paymentExtrasOnTheFly([null,{"name":"enquiryForm","qid":"1","text":"Enquiry Form","type":"control_head"},{"name":"submit","qid":"2","text":"Submit","type":"control_button"},null,null,null,null,null,null,null,null,null,null,null,null,{"name":"email15","qid":"15","subLabel":"","text":"E-mail","type":"control_email"},{"name":"contactNumber","qid":"16","text":"Contact Number","type":"control_phone"},null,{"name":"name","qid":"18","text":"Name","type":"control_textbox"},{"name":"address19","qid":"19","text":"Address","type":"control_address"},null,null,null,null,null,null,null,null,null,null,{"description":"","name":"date","qid":"30","text":"Start Date","type":"control_datetime"},{"description":"","name":"date31","qid":"31","text":"End Date","type":"control_datetime"},{"name":"divider","qid":"32","type":"control_divider"},{"description":"","name":"typeA","qid":"33","subLabel":"","text":"Service For","type":"control_textbox"},{"description":"","name":"typeA34","qid":"34","subLabel":"","text":"Guest ","type":"control_spinner"}]);}, 20); 
</script>
</head>
<body>
<form class="jotform-form" action="https://submit.jotform.com/submit/200221584070038/" method="post" name="form_200221584070038" id="200221584070038" accept-charset="utf-8" autocomplete="on">
  <input type="hidden" name="formID" value="200221584070038">
  <input type="hidden" id="JWTContainer" value="">
  <input type="hidden" id="cardinalOrderNumber" value="">
  <div role="main" class="form-all">
    <ul class="form-section page-section">
      <li id="cid_1" class="form-input-wide" data-type="control_head">
        <div class="form-header-group ">
          <div class="header-text httac htvam">
            <h1 id="header_1" class="form-header" data-component="header">
              Enquiry Form
            </h1>
          </div>
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_textbox" id="id_18">
        <label class="form-label form-label-left form-label-auto" id="label_18" for="input_18">
          Name
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_18" class="form-input jf-required">
          <input type="text" id="input_18" name="name" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" placeholder=" " data-component="textbox" aria-labelledby="label_18" required="">
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_email" id="id_15">
        <label class="form-label form-label-left form-label-auto" id="label_15" for="input_15">
          E-mail
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_15" class="form-input jf-required">
          <input type="email" id="input_15" name="email" class="form-textbox validate[required, Email]" size="30" value="" placeholder=" " data-component="email" aria-labelledby="label_15" required="">
        </div>
      </li>
      <li class="form-line jf-required" data-type="control_phone" id="id_16">
        <label class="form-label form-label-left form-label-auto" id="label_16" for="input_16_area">
          Contact Number
          <span class="form-required">
            *
          </span>
        </label>
        <div id="cid_16" class="form-input jf-required">
          <div data-wrapper-react="true">
            <span class="form-sub-label-container " style="vertical-align:top">
              <input type="tel" id="input_16_area" name="q16_contactNumber[area]" class="form-textbox validate[required]" size="6" value="" data-component="areaCode" aria-labelledby="label_16 sublabel_16_area" required="">
              <span class="phone-separate" aria-hidden="true">
                 -
              </span>
              <label class="form-sub-label" for="input_16_area" id="sublabel_16_area" style="min-height:13px"> Area Code </label>
            </span>
            <span class="form-sub-label-container " style="vertical-align:top">
              <input type="tel" id="input_16_phone" name="phno" class="form-textbox validate[required]" size="12" value="" data-component="phone" aria-labelledby="label_16 sublabel_16_phone" required="">
              <label class="form-sub-label" for="input_16_phone" id="sublabel_16_phone" style="min-height:13px"> Phone Number </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_address" id="id_19">
        <label class="form-label form-label-left form-label-auto" id="label_19" for="input_19_addr_line1"> Address </label>
        <div id="cid_19" class="form-input">
          <table summary="" class="form-address-table">
            <tbody>
              <tr>
                <td colspan="2">
                  <span class="form-sub-label-container " style="vertical-align:top">
                    <input type="text" id="input_19_addr_line1" name="address" class="form-textbox form-address-line" autocomplete="address-line1" value="" data-component="address_line_1" aria-labelledby="label_19 sublabel_19_addr_line1">
                    <label class="form-sub-label" for="input_19_addr_line1" id="sublabel_19_addr_line1" style="min-height:13px"> Street Address </label>
                  </span>
                </td>
              </tr>
              
                <td>
                  <span class="form-sub-label-container " style="vertical-align:top">
                    <input type="text" id="input_19_city" name="city" class="form-textbox form-address-city" autocomplete="address-level2" size="21" value="" data-component="city" aria-labelledby="label_19 sublabel_19_city">
                    <label class="form-sub-label" for="input_19_city" id="sublabel_19_city" style="min-height:13px"> City </label>
                  </span>
                </td>
                <td>
                  <span class="form-sub-label-container " style="vertical-align:top">
                    <input type="text" id="input_19_state" name="state" class="form-textbox form-address-state" autocomplete="address-level1" size="22" value="" data-component="state" aria-labelledby="label_19 sublabel_19_state">
                    <label class="form-sub-label" for="input_19_state" id="sublabel_19_state" style="min-height:13px"> State / Province </label>
                  </span>
                </td>
              </tr>
              <tr>
                <td>
                  <span class="form-sub-label-container " style="vertical-align:top">
                    <input type="text" id="input_19_postal" name="pin" class="form-textbox form-address-postal" autocomplete="postal-code" size="10" value="" data-component="zip" aria-labelledby="label_19 sublabel_19_postal">
                    <label class="form-sub-label" for="input_19_postal" id="sublabel_19_postal" style="min-height:13px"> Postal / Zip Code </label>
                  </span>
                </td>
                <td>
                  <span class="form-sub-label-container " style="vertical-align:top">
                    <select class="form-dropdown form-address-country noTranslate" name="country" id="input_19_country" data-component="country" aria-labelledby="label_19 sublabel_19_country" autocomplete="country">
                      <option value=""> Please Select </option>
                      <option value="United States"> United States </option>
                      <option value="Afghanistan"> Afghanistan </option>
                      <option value="Albania"> Albania </option>
                      <option value="Algeria"> Algeria </option>
                      <option value="American Samoa"> American Samoa </option>
                      <option value="Andorra"> Andorra </option>
                      <option value="Angola"> Angola </option>
                      <option value="Anguilla"> Anguilla </option>
                      <option value="Antigua and Barbuda"> Antigua and Barbuda </option>
                      <option value="Argentina"> Argentina </option>
                      <option value="Armenia"> Armenia </option>
                      <option value="Aruba"> Aruba </option>
                      <option value="Australia"> Australia </option>
                      <option value="Austria"> Austria </option>
                      <option value="Azerbaijan"> Azerbaijan </option>
                      <option value="The Bahamas"> The Bahamas </option>
                      <option value="Bahrain"> Bahrain </option>
                      <option value="Bangladesh"> Bangladesh </option>
                      <option value="Barbados"> Barbados </option>
                      <option value="Belarus"> Belarus </option>
                      <option value="Belgium"> Belgium </option>
                      <option value="Belize"> Belize </option>
                      <option value="Benin"> Benin </option>
                      <option value="Bermuda"> Bermuda </option>
                      <option value="Bhutan"> Bhutan </option>
                      <option value="Bolivia"> Bolivia </option>
                      <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
                      <option value="Botswana"> Botswana </option>
                      <option value="Brazil"> Brazil </option>
                      <option value="Brunei"> Brunei </option>
                      <option value="Bulgaria"> Bulgaria </option>
                      <option value="Burkina Faso"> Burkina Faso </option>
                      <option value="Burundi"> Burundi </option>
                      <option value="Cambodia"> Cambodia </option>
                      <option value="Cameroon"> Cameroon </option>
                      <option value="Canada"> Canada </option>
                      <option value="Cape Verde"> Cape Verde </option>
                      <option value="Cayman Islands"> Cayman Islands </option>
                      <option value="Central African Republic"> Central African Republic </option>
                      <option value="Chad"> Chad </option>
                      <option value="Chile"> Chile </option>
                      <option value="China"> China </option>
                      <option value="Christmas Island"> Christmas Island </option>
                      <option value="Cocos (Keeling) Islands"> Cocos (Keeling) Islands </option>
                      <option value="Colombia"> Colombia </option>
                      <option value="Comoros"> Comoros </option>
                      <option value="Congo"> Congo </option>
                      <option value="Cook Islands"> Cook Islands </option>
                      <option value="Costa Rica"> Costa Rica </option>
                      <option value="Cote d&#x27;Ivoire"> Cote d&#x27;Ivoire </option>
                      <option value="Croatia"> Croatia </option>
                      <option value="Cuba"> Cuba </option>
                      <option value="Cyprus"> Cyprus </option>
                      <option value="Czech Republic"> Czech Republic </option>
                      <option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option>
                      <option value="Denmark"> Denmark </option>
                      <option value="Djibouti"> Djibouti </option>
                      <option value="Dominica"> Dominica </option>
                      <option value="Dominican Republic"> Dominican Republic </option>
                      <option value="Ecuador"> Ecuador </option>
                      <option value="Egypt"> Egypt </option>
                      <option value="El Salvador"> El Salvador </option>
                      <option value="Equatorial Guinea"> Equatorial Guinea </option>
                      <option value="Eritrea"> Eritrea </option>
                      <option value="Estonia"> Estonia </option>
                      <option value="Ethiopia"> Ethiopia </option>
                      <option value="Falkland Islands"> Falkland Islands </option>
                      <option value="Faroe Islands"> Faroe Islands </option>
                      <option value="Fiji"> Fiji </option>
                      <option value="Finland"> Finland </option>
                      <option value="France"> France </option>
                      <option value="French Polynesia"> French Polynesia </option>
                      <option value="Gabon"> Gabon </option>
                      <option value="The Gambia"> The Gambia </option>
                      <option value="Georgia"> Georgia </option>
                      <option value="Germany"> Germany </option>
                      <option value="Ghana"> Ghana </option>
                      <option value="Gibraltar"> Gibraltar </option>
                      <option value="Greece"> Greece </option>
                      <option value="Greenland"> Greenland </option>
                      <option value="Grenada"> Grenada </option>
                      <option value="Guadeloupe"> Guadeloupe </option>
                      <option value="Guam"> Guam </option>
                      <option value="Guatemala"> Guatemala </option>
                      <option value="Guernsey"> Guernsey </option>
                      <option value="Guinea"> Guinea </option>
                      <option value="Guinea-Bissau"> Guinea-Bissau </option>
                      <option value="Guyana"> Guyana </option>
                      <option value="Haiti"> Haiti </option>
                      <option value="Honduras"> Honduras </option>
                      <option value="Hong Kong"> Hong Kong </option>
                      <option value="Hungary"> Hungary </option>
                      <option value="Iceland"> Iceland </option>
                      <option value="India"> India </option>
                      <option value="Indonesia"> Indonesia </option>
                      <option value="Iran"> Iran </option>
                      <option value="Iraq"> Iraq </option>
                      <option value="Ireland"> Ireland </option>
                      <option value="Israel"> Israel </option>
                      <option value="Italy"> Italy </option>
                      <option value="Jamaica"> Jamaica </option>
                      <option value="Japan"> Japan </option>
                      <option value="Jersey"> Jersey </option>
                      <option value="Jordan"> Jordan </option>
                      <option value="Kazakhstan"> Kazakhstan </option>
                      <option value="Kenya"> Kenya </option>
                      <option value="Kiribati"> Kiribati </option>
                      <option value="North Korea"> North Korea </option>
                      <option value="South Korea"> South Korea </option>
                      <option value="Kosovo"> Kosovo </option>
                      <option value="Kuwait"> Kuwait </option>
                      <option value="Kyrgyzstan"> Kyrgyzstan </option>
                      <option value="Laos"> Laos </option>
                      <option value="Latvia"> Latvia </option>
                      <option value="Lebanon"> Lebanon </option>
                      <option value="Lesotho"> Lesotho </option>
                      <option value="Liberia"> Liberia </option>
                      <option value="Libya"> Libya </option>
                      <option value="Liechtenstein"> Liechtenstein </option>
                      <option value="Lithuania"> Lithuania </option>
                      <option value="Luxembourg"> Luxembourg </option>
                      <option value="Macau"> Macau </option>
                      <option value="Macedonia"> Macedonia </option>
                      <option value="Madagascar"> Madagascar </option>
                      <option value="Malawi"> Malawi </option>
                      <option value="Malaysia"> Malaysia </option>
                      <option value="Maldives"> Maldives </option>
                      <option value="Mali"> Mali </option>
                      <option value="Malta"> Malta </option>
                      <option value="Marshall Islands"> Marshall Islands </option>
                      <option value="Martinique"> Martinique </option>
                      <option value="Mauritania"> Mauritania </option>
                      <option value="Mauritius"> Mauritius </option>
                      <option value="Mayotte"> Mayotte </option>
                      <option value="Mexico"> Mexico </option>
                      <option value="Micronesia"> Micronesia </option>
                      <option value="Moldova"> Moldova </option>
                      <option value="Monaco"> Monaco </option>
                      <option value="Mongolia"> Mongolia </option>
                      <option value="Montenegro"> Montenegro </option>
                      <option value="Montserrat"> Montserrat </option>
                      <option value="Morocco"> Morocco </option>
                      <option value="Mozambique"> Mozambique </option>
                      <option value="Myanmar"> Myanmar </option>
                      <option value="Nagorno-Karabakh"> Nagorno-Karabakh </option>
                      <option value="Namibia"> Namibia </option>
                      <option value="Nauru"> Nauru </option>
                      <option value="Nepal"> Nepal </option>
                      <option value="Netherlands"> Netherlands </option>
                      <option value="Netherlands Antilles"> Netherlands Antilles </option>
                      <option value="New Caledonia"> New Caledonia </option>
                      <option value="New Zealand"> New Zealand </option>
                      <option value="Nicaragua"> Nicaragua </option>
                      <option value="Niger"> Niger </option>
                      <option value="Nigeria"> Nigeria </option>
                      <option value="Niue"> Niue </option>
                      <option value="Norfolk Island"> Norfolk Island </option>
                      <option value="Turkish Republic of Northern Cyprus"> Turkish Republic of Northern Cyprus </option>
                      <option value="Northern Mariana"> Northern Mariana </option>
                      <option value="Norway"> Norway </option>
                      <option value="Oman"> Oman </option>
                      <option value="Pakistan"> Pakistan </option>
                      <option value="Palau"> Palau </option>
                      <option value="Palestine"> Palestine </option>
                      <option value="Panama"> Panama </option>
                      <option value="Papua New Guinea"> Papua New Guinea </option>
                      <option value="Paraguay"> Paraguay </option>
                      <option value="Peru"> Peru </option>
                      <option value="Philippines"> Philippines </option>
                      <option value="Pitcairn Islands"> Pitcairn Islands </option>
                      <option value="Poland"> Poland </option>
                      <option value="Portugal"> Portugal </option>
                      <option value="Puerto Rico"> Puerto Rico </option>
                      <option value="Qatar"> Qatar </option>
                      <option value="Republic of the Congo"> Republic of the Congo </option>
                      <option value="Romania"> Romania </option>
                      <option value="Russia"> Russia </option>
                      <option value="Rwanda"> Rwanda </option>
                      <option value="Saint Barthelemy"> Saint Barthelemy </option>
                      <option value="Saint Helena"> Saint Helena </option>
                      <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
                      <option value="Saint Lucia"> Saint Lucia </option>
                      <option value="Saint Martin"> Saint Martin </option>
                      <option value="Saint Pierre and Miquelon"> Saint Pierre and Miquelon </option>
                      <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
                      <option value="Samoa"> Samoa </option>
                      <option value="San Marino"> San Marino </option>
                      <option value="Sao Tome and Principe"> Sao Tome and Principe </option>
                      <option value="Saudi Arabia"> Saudi Arabia </option>
                      <option value="Senegal"> Senegal </option>
                      <option value="Serbia"> Serbia </option>
                      <option value="Seychelles"> Seychelles </option>
                      <option value="Sierra Leone"> Sierra Leone </option>
                      <option value="Singapore"> Singapore </option>
                      <option value="Slovakia"> Slovakia </option>
                      <option value="Slovenia"> Slovenia </option>
                      <option value="Solomon Islands"> Solomon Islands </option>
                      <option value="Somalia"> Somalia </option>
                      <option value="Somaliland"> Somaliland </option>
                      <option value="South Africa"> South Africa </option>
                      <option value="South Ossetia"> South Ossetia </option>
                      <option value="South Sudan"> South Sudan </option>
                      <option value="Spain"> Spain </option>
                      <option value="Sri Lanka"> Sri Lanka </option>
                      <option value="Sudan"> Sudan </option>
                      <option value="Suriname"> Suriname </option>
                      <option value="Svalbard"> Svalbard </option>
                      <option value="eSwatini"> eSwatini </option>
                      <option value="Sweden"> Sweden </option>
                      <option value="Switzerland"> Switzerland </option>
                      <option value="Syria"> Syria </option>
                      <option value="Taiwan"> Taiwan </option>
                      <option value="Tajikistan"> Tajikistan </option>
                      <option value="Tanzania"> Tanzania </option>
                      <option value="Thailand"> Thailand </option>
                      <option value="Timor-Leste"> Timor-Leste </option>
                      <option value="Togo"> Togo </option>
                      <option value="Tokelau"> Tokelau </option>
                      <option value="Tonga"> Tonga </option>
                      <option value="Transnistria Pridnestrovie"> Transnistria Pridnestrovie </option>
                      <option value="Trinidad and Tobago"> Trinidad and Tobago </option>
                      <option value="Tristan da Cunha"> Tristan da Cunha </option>
                      <option value="Tunisia"> Tunisia </option>
                      <option value="Turkey"> Turkey </option>
                      <option value="Turkmenistan"> Turkmenistan </option>
                      <option value="Turks and Caicos Islands"> Turks and Caicos Islands </option>
                      <option value="Tuvalu"> Tuvalu </option>
                      <option value="Uganda"> Uganda </option>
                      <option value="Ukraine"> Ukraine </option>
                      <option value="United Arab Emirates"> United Arab Emirates </option>
                      <option value="United Kingdom"> United Kingdom </option>
                      <option value="Uruguay"> Uruguay </option>
                      <option value="Uzbekistan"> Uzbekistan </option>
                      <option value="Vanuatu"> Vanuatu </option>
                      <option value="Vatican City"> Vatican City </option>
                      <option value="Venezuela"> Venezuela </option>
                      <option value="Vietnam"> Vietnam </option>
                      <option value="British Virgin Islands"> British Virgin Islands </option>
                      <option value="Isle of Man"> Isle of Man </option>
                      <option value="US Virgin Islands"> US Virgin Islands </option>
                      <option value="Wallis and Futuna"> Wallis and Futuna </option>
                      <option value="Western Sahara"> Western Sahara </option>
                      <option value="Yemen"> Yemen </option>
                      <option value="Zambia"> Zambia </option>
                      <option value="Zimbabwe"> Zimbabwe </option>
                      <option value="other"> Other </option>
                    </select>
                    <label class="form-sub-label" for="input_19_country" id="sublabel_19_country" style="min-height:13px"> Country </label>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </li>
      <li class="form-line" data-type="control_divider" id="id_32">
        <div id="cid_32" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:5px">
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_textbox" id="id_33">
        <label class="form-label form-label-left form-label-auto" id="label_33" for="input_33"> Service For </label>
        <div id="cid_33" class="form-input">
          <input type="text" id="input_33" name="service_for" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_33">
        </div>
      </li>
      <li class="form-line" data-type="control_datetime" id="id_30">
        <label class="form-label form-label-left form-label-auto" id="label_30" for="lite_mode_30"> Start Date </label>
        <div id="cid_30" class="form-input">
          <div data-wrapper-react="true">
            <div style="display:none">
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="month_30" name="month1" size="2" data-maxlength="2" value="" aria-labelledby="label_30 sublabel_30_month">
                <span class="date-separate" aria-hidden="true">
                   -
                </span>
                <label class="form-sub-label" for="month_30" id="sublabel_30_month" style="min-height:13px"> Month </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="day_30" name="day1" size="2" data-maxlength="2" value="" aria-labelledby="label_30 sublabel_30_day">
                <span class="date-separate" aria-hidden="true">
                   -
                </span>
                <label class="form-sub-label" for="day_30" id="sublabel_30_day" style="min-height:13px"> Day </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="year_30" name="year1" size="4" data-maxlength="4" value="" aria-labelledby="label_30 sublabel_30_year">
                <label class="form-sub-label" for="year_30" id="sublabel_30_year" style="min-height:13px"> Year </label>
              </span>
            </div>
            <span class="form-sub-label-container " style="vertical-align:top">
              <input type="text" class="form-textbox validate[limitDate, validateLiteDate]" id="lite_mode_30" size="12" data-maxlength="12" data-age="" value="" data-format="mmddyyyy" data-seperator="-" placeholder="mm-dd-yyyy" aria-labelledby="label_30">
              <img alt="Pick a Date" id="input_30_pick" src="https://cdn.jotfor.ms/images/calendar.png" style="vertical-align:middle;margin-left:5px" data-component="datetime" aria-hidden="true">
              <label class="form-sub-label" for="lite_mode_30" id="sublabel_30_litemode" style="min-height:13px">  </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_datetime" id="id_31">
        <label class="form-label form-label-left form-label-auto" id="label_31" for="lite_mode_31"> End Date </label>
        <div id="cid_31" class="form-input">
          <div data-wrapper-react="true">
            <div style="display:none">
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="month_31" name="month2" size="2" data-maxlength="2" value="" aria-labelledby="label_31 sublabel_31_month">
                <span class="date-separate" aria-hidden="true">
                   -
                </span>
                <label class="form-sub-label" for="month_31" id="sublabel_31_month" style="min-height:13px"> Month </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="day_31" name="day2" size="2" data-maxlength="2" value="" aria-labelledby="label_31 sublabel_31_day">
                <span class="date-separate" aria-hidden="true">
                   -
                </span>
                <label class="form-sub-label" for="day_31" id="sublabel_31_day" style="min-height:13px"> Day </label>
              </span>
              <span class="form-sub-label-container " style="vertical-align:top">
                <input type="tel" class="form-textbox validate[limitDate]" id="year_31" name="year2" size="4" data-maxlength="4" value="" aria-labelledby="label_31 sublabel_31_year">
                <label class="form-sub-label" for="year_31" id="sublabel_31_year" style="min-height:13px"> Year </label>
              </span>
            </div>
            <span class="form-sub-label-container " style="vertical-align:top">
              <input type="text" class="form-textbox validate[limitDate, validateLiteDate]" id="lite_mode_31" size="12" data-maxlength="12" data-age="" value="" data-format="mmddyyyy" data-seperator="-" placeholder="mm-dd-yyyy" aria-labelledby="label_31">
              <img alt="Pick a Date" id="input_31_pick" src="https://cdn.jotfor.ms/images/calendar.png" style="vertical-align:middle;margin-left:5px" data-component="datetime" aria-hidden="true">
              <label class="form-sub-label" for="lite_mode_31" id="sublabel_31_litemode" style="min-height:13px">  </label>
            </span>
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_spinner" id="id_34">
        <label class="form-label form-label-left form-label-auto" id="label_34" for="input_34"> Guest </label>
        <div id="cid_34" class="form-input">
          <div data-wrapper-react="true">
            <input type="number" id="input_34" name="guest_number" data-type="input-spinner" class="form-spinner-input  form-textbox" value="" data-component="spinner" aria-labelledby="label_34">
          </div>
        </div>
      </li>
      <li class="form-line" data-type="control_button" id="id_2">
        <div id="cid_2" class="form-input-wide">
          <div style="margin-left:156px" class="form-buttons-wrapper ">
            <button id="input_2" type="submit" class="form-submit-button" data-component="button" data-content="">
              Submit
            </button>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="">
      </li>
    </ul>
  </div>
  <script>
  JotForm.showJotFormPowered = "new_footer";
  </script>
  <input type="hidden" id="simple_spc" name="simple_spc" value="200221584070038">
  <script type="text/javascript">
  document.getElementById("si" + "mple" + "_spc").value = "200221584070038-200221584070038";
  </script>
  <div class="formFooter-heightMask">
  </div>
  
</form></body>
</html>
