@extends('layouts.adminmaster')
@section('body')
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">All Mobile Accessories</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Data Tables</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Mobile Accessories</li>
                            </ol>
                        </div>
                    </div>
                  
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-topline-purple">
                                        <div class="card-head">
                                            <header>STRIPED TABLE</header>
                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
			                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
			                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                        <div class="table-responsive">
                                            <table class="table table-striped custom-table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Name</th>
                                                        <th>Price</th>
                                                        <th>Descrition</th>
                                                        <th>image</th>
                                                       
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($mobile as $mobile)
                                                    <tr>
                                                    <td><a href="#">{{$mobile->title}}</a>
                                                        </td>
                                                        <td>{{$mobile->price}}</td>
                                                        <td>{{$mobile->description}}</td>


                                                        @php($id=$mobile->id)
                                                        @php($mobileimage=DB::table('mobileimages')->where('mobile_id',$id)->limit(1)->get())
                                                        @foreach($mobileimage as $mobileimage)
                                                        <td><img src="upload/{{$mobileimage->image}}" style="height:120px; width:150px"></td>
                                                        @endforeach


                                                        <td >
                                                            <!--<button class="btn btn-success btn-xs">
                                                                <i class="fa fa-check"></i>
                                                            </button>
                                                            <button class="btn btn-primary btn-xs">
                                                                <i class="fa fa-pencil"></i>
                                                            </button>-->
                                                        <a href="/del_mobile?id={{$mobile->id}}"> <button class="btn btn-danger btn-xs">
                                                                <i class="fa fa-trash-o "></i>
                                                            </button>
                                                        </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                   
                                      
                                                   
                                                   
                                                    
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
 @endsection