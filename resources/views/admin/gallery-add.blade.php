@extends('layouts.adminmaster')
@section('body')
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Add Image</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                               
                                <li class="active"> Add Image</li>
                            </ol>
                        </div>
                    </div>
                    
                   <form action="/add_gal" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-6">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control">
                        </div>

                        <div class="col-6">
                            <label>Image</label>
                            <input type="file" name="image" class="form-control">
                        </div>
                    </div>

                    
                    <br>
                     <input type="hidden" value="<?php echo csrf_token(); ?>" name="_token">
                    <div class="row">
                        <div class="col-12">
                            
                            <input type="submit" name="submit" value="Submit" class="btn btn-info">
                        </div>

                    </div>
                   </form>
                   
                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
 @endsection