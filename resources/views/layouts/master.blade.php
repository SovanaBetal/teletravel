<!DOCTYPE html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="YcPobmrDSOSlAhzryYX0nxbq8TXzC4vlE7y5cWQE">
<link rel="icon" type="image/png" href="uploads\demo\general\favicon.png">
<title>Home Page</title>
<meta name="description" content="">
<meta property="og:url" content="https://bookingcore.org">
<meta property="og:type" content="article">
<meta property="og:title" content="Home Page">
<meta property="og:description" content="">
<meta property="og:image" content="">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Home Page">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="">
<link rel="canonical" href="index.htm">
<link href="libs\bootstrap\css\bootstrap.css" rel="stylesheet">
<link href="libs\font-awesome\css\font-awesome.css" rel="stylesheet">
<link href="libs\ionicons\css\ionicons.min.css" rel="stylesheet">
<link href="libs\icofont\icofont.min.css" rel="stylesheet">
<link href="libs\select2\css\select2.min.css" rel="stylesheet">
<link href="css\app.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="libs\daterange\daterangepicker.css">

<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link rel='stylesheet' id='google-font-css-css' href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600' type='text/css' media='all'>
<script>
        var bookingCore = {
            url:'https://bookingcore.org/en',
            url_root:'https://bookingcore.org',
            booking_decimals:0,
            thousand_separator:'.',
            decimal_separator:',',
            currency_position:'left',
            currency_symbol:'$',
			currency_rate:'1',
            date_format:'MM/DD/YYYY',
            map_provider:'gmap',
            map_gmap_key:'',
            routes:{
                login:'https://bookingcore.org/en/login',
                register:'https://bookingcore.org/en/register',
            },
            currentUser:0
        };
        var i18n = {
            warning:"Warning",
            success:"Success",
        };
        var daterangepickerLocale = {
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October}",
                "November",
                "December"
            ],
        };
    </script>
    <link href="module\location\css\location.css?_ver=1.5.1" rel="stylesheet">
<link href="css\contact.css?_ver=1.5.1" rel="stylesheet">
<link href="module\news\css\news.css?_ver=1.5.1" rel="stylesheet">
<link href="css\app-1.css?_ver=1.5.1" rel="stylesheet">
<link href="module\tour\css\tour.css?_ver=1.5.1" rel="stylesheet">
<link href="module\car\css\car.css?_ver=1.5.1" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="libs\ion_rangeslider\css\ion.rangeSlider.min.css">
<link rel="stylesheet" type="text/css" href="libs\fotorama\fotorama.css">
<style id="custom-css">

    a,
    .bravo-news .btn-readmore,
    .bravo_wrap .bravo_header .content .header-left .bravo-menu ul li:hover > a,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .field-icon,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .render,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .field-detination #dropdown-destination .form-control,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .btn-apply-price-range,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .btn-more-item,
    .input-number-group i,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .field-icon,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .field-detination #dropdown-destination .form-control,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .render,
    .hotel_rooms_form .form-search-rooms .form-search-row>div .form-group .render,
    .bravo_wrap .bravo_form .form-content .render,
    a:hover {
        color: #5191fa;
    }
    .bravo-pagination ul li.active a, .bravo-pagination ul li.active span
    {
        color:#5191fa!important;
    }
    .bravo-news .widget_category ul li span,
    .bravo_wrap .bravo_search_tour .bravo_form_search .bravo_form .g-button-submit button,
    .bravo_wrap .bravo_search_tour .bravo_filter .filter-title:before,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-bar,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single,
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-handle>i:first-child,
    .bravo-news .header .cate ul li,
    .bravo_wrap .page-template-content .bravo-form-search-tour .bravo_form_search_tour .g-button-submit button,
    .bravo_wrap .page-template-content .bravo-list-locations .list-item .destination-item .image .content .desc,
    .bravo_wrap .bravo_detail_space .bravo_content .g-attributes h3:after,
    .bravo_wrap .bravo_form .g-button-submit button,
    .btn.btn-primary,
    .bravo_wrap .bravo_form .g-button-submit button:active,
    .btn.btn-primary:active,
    .bravo_wrap .bravo_detail_space .bravo-list-hotel-related-widget .heading:after,
    .btn-primary:not(:disabled):not(.disabled):active
    {
        background: #5191fa;
    }

    .bravo-pagination ul li.active a, .bravo-pagination ul li.active span
    {
        border-color:#5191fa!important;
    }
    .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from:before, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to:before, .bravo_wrap .bravo_search_tour .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single:before,
    .bravo-reviews .review-form .form-wrapper,
    .bravo_wrap .bravo_detail_tour .bravo_content .bravo_tour_book
    {
        border-top-color:#5191fa;
    }

    .bravo_wrap .bravo_footer .main-footer .nav-footer .context .contact{
        border-left-color:#5191fa;
    }
    .hotel_rooms_form .form-search-rooms{
        border-bottom-color:#5191fa;
    }

    .bravo_wrap .bravo_form .field-icon,
    .bravo_wrap .bravo_form .smart-search .parent_text,
    .bravo_wrap .bravo_form .smart-search:after,
    .bravo_wrap .bravo_form .dropdown-toggle:after,
    .bravo_wrap .page-template-content .bravo-list-space .item-loop .service-review .rate,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .btn-more-item,
    .bravo_wrap .bravo_detail_space .bravo_content .g-header .review-score .head .left .text-rating,
    .bravo-reviews .review-box .review-box-score .review-score,
    .bravo-reviews .review-box .review-box-score .review-score-base span,
    .bravo_wrap .bravo_detail_tour .bravo_content .g-header .review-score .head .left .text-rating
    {
        color: #5191fa;
    }

    .bravo_wrap .bravo_form .smart-search .parent_text::-webkit-input-placeholder{

        color: #5191fa;
    }
    .bravo_wrap .bravo_form .smart-search .parent_text::-moz-placeholder{

        color: #5191fa;
    }
    .bravo_wrap .bravo_form .smart-search .parent_text::-ms-input-placeholder{

        color: #5191fa;
    }
    .bravo_wrap .bravo_form .smart-search .parent_text::-moz-placeholder{

        color: #5191fa;
    }
    .bravo_wrap .bravo_form .smart-search .parent_text::placeholder{

        color: #5191fa;
    }


    .bravo_wrap .bravo_search_space .bravo-list-item .list-item .item-loop .service-review .rate,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .btn-apply-price-range{

        color: #5191fa;
    }
    .bravo_wrap .page-template-content .bravo-list-locations.style_2 .list-item .destination-item:hover .title,
    .bravo_wrap .page-template-content .bravo-list-space .item-loop .sale_info,
    .bravo_wrap .bravo_search_space .bravo-list-item .list-item .item-loop .sale_info,
    .bravo_wrap .bravo_search_space .bravo_filter .filter-title:before,
    .bravo_wrap .bravo_detail_space .bravo_content .g-header .review-score .head .score,
    .bravo-reviews .review-form .btn,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-bar,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single,
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-handle>i:first-child
    {
        background: #5191fa;
    }
    .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from:before, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to:before, .bravo_wrap .bravo_search_space .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single:before {
        border-top-color: #5191fa;
    }

    .bravo_wrap .bravo_detail_space .bravo_content .g-overview ul li:before {
        border: 1px solid #5191fa;
    }

    .bravo_wrap .bravo_detail_space .bravo-list-space-related .item-loop .sale_info {
        background-color: #5191fa;
    }

    .bravo_wrap .bravo_detail_space .bravo_content .g-header .review-score .head .score::after {
        border-bottom: 25px solid #5191fa;
    }

    .bravo_wrap .bravo_detail_space .bravo_content .bravo_space_book {
        border-top: 5px solid #5191fa;
    }

    body .daterangepicker.loading:after {
        color: #5191fa;
    }

    body .daterangepicker .drp-calendar .calendar-table tbody tr td.end-date {
        border-right: solid 2px #5191fa;
    }
    body .daterangepicker .drp-calendar .calendar-table tbody tr td.start-date {
        border-left: solid 2px #5191fa;
    }
    .bravo_wrap .bravo_detail_space .bravo-list-space-related .item-loop .service-review .rate {
        color: #5191fa;
    }

    .has-search-map .bravo-filter-price .irs--flat .irs-bar,
    .has-search-map .bravo-filter-price .irs--flat .irs-handle>i:first-child,
    .has-search-map .bravo-filter-price .irs--flat .irs-from, .has-search-map .bravo-filter-price .irs--flat .irs-to, .has-search-map .bravo-filter-price .irs--flat .irs-single {
        background-color: #5191fa;
    }

    .has-search-map .bravo-filter-price .irs--flat .irs-from:before, .has-search-map .bravo-filter-price .irs--flat .irs-to:before, .has-search-map .bravo-filter-price .irs--flat .irs-single:before {
        border-top-color: #5191fa;
    }

    .bravo_wrap .bravo_detail_tour .bravo_content .g-header .review-score .head .score {
        background: #5191fa;
    }
    .bravo_wrap .bravo_detail_tour .bravo_content .g-header .review-score .head .score::after {
        border-bottom: 25px solid #5191fa;
    }

    .bravo_wrap .bravo_detail_tour .bravo_content .g-overview ul li:before {
        border: 1px solid #5191fa;
    }

    .bravo_wrap .bravo_detail_location .bravo_content .g-location-module .location-module-nav li a.active {
        border-bottom: 1px solid #5191fa;
        color: #5191fa;
    }

    .bravo_wrap .bravo_detail_location .bravo_content .g-location-module .item-loop .sale_info {
        background-color: #5191fa;
    }
    .bravo_wrap .page-template-content .bravo-featured-item.style2 .number-circle {
        border: 2px solid #5191fa;
        color: #5191fa;
    }
    .bravo_wrap .page-template-content .bravo-featured-item.style3 .featured-item:hover {
        border-color: #5191fa;
    }

    .booking-success-notice .booking-info-detail {
        border-left: 3px solid #5191fa;
    }
    .bravo_wrap .bravo_detail_tour .bravo_single_book,
    .bravo_wrap .bravo_detail_space .bravo_single_book {
        border-top: 5px solid#5191fa;
    }
    .bravo_wrap .page-template-content .bravo-form-search-all .g-form-control .nav-tabs li a.active {
        background-color: #5191fa;
        border-color: #5191fa;
    }

    .bravo_wrap .bravo_detail_location .bravo_content .g-location-module .item-loop .service-review .rate,
    .bravo_wrap .bravo_detail_location .bravo_content .g-trip-ideas .trip-idea .trip-idea-category,
    .bravo_wrap .bravo_footer .main-footer .nav-footer .context ul li a:hover,
    .bravo_wrap .bravo_detail_tour .bravo_content .g-attributes .list-attributes .item i,
    .bravo_wrap .bravo_detail_space .bravo_content .g-attributes .list-attributes .item i,
    .bravo_wrap .page-template-content .bravo-list-hotel .item-loop .service-review .rate,
    .bravo_wrap .page-template-content .bravo-list-tour.box_shadow .list-item .item .caption .title-address .title a:hover,
    .bravo_wrap .bravo_search_hotel .bravo-list-item .list-item .item-loop .service-review .rate,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .btn-apply-price-range {
        color: #5191fa;
    }

    .bravo_wrap .bravo_detail_tour .bravo-list-tour-related .item-tour .featured ,
    .bravo_wrap .bravo_search_tour .bravo-list-item .list-item .item-tour .featured,
    .bravo_wrap .page-template-content .bravo-list-tour .item-tour .featured,
    .bravo_wrap .bravo_search_hotel .bravo_filter .filter-title:before {
        background: #5191fa;
    }
    .bravo_wrap .page-template-content .bravo-list-tour.box_shadow .list-item .item .header-thumb .tour-book-now,
    .bravo_wrap .bravo_search_hotel .bravo-list-item .list-item .item-loop .sale_info,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-bar,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-handle>i:first-child {
        background-color: #5191fa;
    }
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-from:before,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-to:before,
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .bravo-filter-price .irs--flat .irs-single:before {
        border-top-color: #5191fa;
    }

    .bravo_wrap .bravo_search_hotel .bravo-list-item .list-item .item-loop-list .service-review-pc .head .score,
    .bravo_wrap .bravo_search_hotel .bravo_content .g-header .review-score .head .score {
        background: #5191fa;
    }

    .bravo_wrap .bravo_search_hotel .bravo_content .g-overview ul li:before {
        border: 1px solid #5191fa;
    }
    .bravo_wrap .bravo_search_hotel .bravo_filter .g-filter-item .item-content .btn-more-item,
    .bravo_wrap .bravo_search_hotel .bravo_content .g-header .review-score .head .left .text-rating,
    .bravo_wrap .bravo_search_hotel .bravo-list-item .list-item .item-loop-list .service-review-pc .head .left .text-rating,
    .bravo_wrap .bravo_detail_hotel  .btn-show-all,
    .bravo_wrap .bravo_detail_hotel  .bravo-list-hotel-related .item-loop .service-review .rate,
    .bravo_wrap .bravo_form .select-guests-dropdown .dropdown-item-row .count-display{
        color: #5191fa;
    }

    .bravo_wrap .bravo_search_hotel .bravo-list-item .list-item .item-loop-list .service-review-pc .head .score::after {
        border-bottom: 15px solid #5191fa;
    }

    .bravo_wrap .bravo_detail_hotel .bravo_content .g-header .review-score .head .score {
        background: #5191fa;
    }
    .bravo_wrap .bravo_detail_hotel .bravo_content .g-header .review-score .head .score:after {
        border-bottom: 25px solid #5191fa;
    }
    .bravo_wrap .bravo_detail_hotel .bravo-list-hotel-related-widget .heading:after {
        background: #5191fa;
    }
    .bravo_wrap .bravo_detail_hotel .bravo_content .g-attributes h3:after {
        background: #5191fa;
    }
    .bravo_wrap .bravo_detail_hotel .bravo_content .g-header .review-score .head .left .text-rating {
        color: #5191fa;
    }
    .bravo_wrap .select-guests-dropdown .dropdown-item-row .count-display {
        color: #5191fa;
    }

    .bravo_wrap .bravo-checkbox input[type=checkbox]:checked+.checkmark:after {
        border: solid #5191fa;
        border-width: 0 2px 2px 0;
    }

    body{
        }

    
    
</style>
<link href="libs\carousel-2\owl.carousel.css" rel="stylesheet">
</head>
<body class="frontend-page ">
<div class="bravo_wrap">
<div class="bravo_topbar">
<div class="container">
<div class="content">
<!--<div class="topbar-left">
<div class="socials">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-linkedin"></i></a>
<a href="#"><i class="fa fa-google-plus"></i></a>
</div>
<span class="line"></span>
<a href="cdn-cgi\l\email-protection.html#5c3f3332283d3f281c3e33333735323b3f332e39723f3331"><span class="__cf_email__" data-cfemail="ea8985849e8b899eaa8885858183848d8985988fc4898587">[email&#160;protected]</span></a>
</div>-->
<div class="topbar-right">
<ul class="topbar-items">
<!--<li class="dropdown">
<a href="#" data-toggle="dropdown" class="is_login">
USD
<i class="fa fa-angle-down"></i>
</a>
<ul class="dropdown-menu text-left width-auto">
<li>
<a href="https://bookingcore.org?set_currency=eur" class="is_login">
EUR
</a>
</li>
<li>
<a href="https://bookingcore.org?set_currency=jpy" class="is_login">
JPY
</a>
</li>
</ul>
</li>-->
<!--<li class="dropdown">
<a href="#" data-toggle="dropdown" class="is_login">
<span class="flag-icon flag-icon-gb"></span>
English
<i class="fa fa-angle-down"></i>
</a>
<ul class="dropdown-menu text-left">
<li>
<a href="index-1.htm?set_lang=ja" class="is_login">
<span class="flag-icon flag-icon-jp"></span>
Japanese
</a>
</li>
</ul>
</li>-->

</ul>
</div>
</div>
</div>
</div>
<div class="bravo_header">
<div class="container">
<div class="content">
<div class="header-left">
<a href="#" class="bravo-logo">
<img src="uploads\teletravel.png" alt="Booking Core">
</a>
<div class="bravo-menu">
<ul class="main-menu menu-generated">
    <li class=" depth-0"><a target="" href="/">Home </a>
       <!-- <ul class="children-menu menu-dropdown">
            <li class=" depth-1"><a target="" href="index.htm">Home Page</a></li>
            <li class=" depth-1"><a target="" href="page\hotel.html">Home Hotel</a></li>
            <li class=" depth-1"><a target="" href="page\tour.html">Home Tour</a></li>
            <li class=" depth-1"><a target="" href="page\space.html">Home Space</a></li>
            <li class=" depth-1"><a target="" href="page\car.html">Home Car</a></li>
        </ul>-->
    </li>
    <li class=" depth-0"><a target="" href="/about-us">About us</a>
        
    </li>
    <li class=" depth-0"><a target="" href="#">Services <i class="fa fa-angle-down"></i></a>
        <ul class="children-menu menu-dropdown">
            <li class=" depth-1"><a target="" href="/tour-and-travel">Tour & Travel</a></li>
            <li class=" depth-1"><a target="" href="/cab-services">Cab Service</a></li>
            <li class=" depth-1"><a target="" href="#">Mobile & Accessories</a></li>
            <li class=" depth-1"><a target="" href="#">Ticket Enquery</a></li>
        </ul>
    </li>
    <li class=" depth-0"><a target="" href="#">Blogs & News <i class="fa fa-angle-down"></i></a>
        <ul class="children-menu menu-dropdown">
            <li class=" depth-1"><a target="" href="#">Blogs</a></li>
            <li class=" depth-1"><a target="" href="/news">News</a></li>
           
        </ul>
    </li>
    <li class=" depth-0"><a target="" href="#">Gallery </a>
        
    </li>
    
    <li class=" depth-0"><a target="" href="/contact">Contact Us</a></li>
</ul> 
</div>
</div>
<div class="header-right">
<button class="bravo-more-menu">
<i class="fa fa-bars"></i>
</button>
</div>
</div>
</div>
<div class="bravo-menu-mobile" style="display:none;">
<div class="user-profile">
<div class="b-close"><i class="icofont-scroll-left"></i></div>
<div class="avatar"></div>
<ul>

</ul>
<!--<ul class="multi-lang">
<li class="dropdown">
<a href="#" data-toggle="dropdown" class="is_login">
USD
<i class="fa fa-angle-down"></i>
</a>
<ul class="dropdown-menu text-left width-auto">
<li>
<a href="https://bookingcore.org?set_currency=eur" class="is_login">
EUR
</a>
</li>
<li>
<a href="https://bookingcore.org?set_currency=jpy" class="is_login">
JPY
</a>
</li>
</ul>
</li>
</ul>-->
<!--<ul class="multi-lang">
<li class="dropdown">
<a href="#" data-toggle="dropdown" class="is_login">
<span class="flag-icon flag-icon-gb"></span>
English
<i class="fa fa-angle-down"></i>
</a>
<ul class="dropdown-menu text-left">
<li>
<a href="index-1.htm?set_lang=ja" class="is_login">
<span class="flag-icon flag-icon-jp"></span>
Japanese
</a>
</li>
</ul>
</li>
</ul>-->
</div>
<div class="g-menu">
<ul class="main-menu menu-generated">
    <li class=" depth-0"><a target="" href="/">Home </a>
        
    </li>
    <li class=" depth-0"><a target="" href="/about-us">About Us </a>
        
    </li>
    <li class=" depth-0"><a target="" href="#">Services <i class="fa fa-angle-down"></i></a>
        <ul class="children-menu menu-dropdown">
            <li class=" depth-1"><a target="" href="">Tour & Travel</a></li>
            <li class=" depth-1"><a target="" href="">Cab Service</a></li>
            <li class=" depth-1"><a target="" href="#">Mobile & Accessories</a></li>
            <li class=" depth-1"><a target="" href="#">Ticket Enquiery</a></li>
        </ul>
    </li>
    <li class=" depth-0"><a target="" href="#">Blogs & News <i class="fa fa-angle-down"></i></a>
        <ul class="children-menu menu-dropdown">
            <li class=" depth-1"><a target="" href="#">Blogs</a></li>
            <li class=" depth-1"><a target="" href="/news">News</a></li>
         
        </ul>
    </li>
    <li class=" depth-0"><a target="" href="#">Car <i class="fa fa-angle-down"></i></a>
        <ul class="children-menu menu-dropdown">
            <li class=" depth-1"><a target="" href="#">Car List</a></li>
            <li class=" depth-1"><a target="" href="#">Car Map</a></li>
            <li class=" depth-1"><a target="" href="#">Car Detail</a></li>
        </ul>
    </li>
    <li class=" depth-0"><a target="" href="#">Pages <i class="fa fa-angle-down"></i></a>
        <ul class="children-menu menu-dropdown">
            <li class=" depth-1"><a target="" href="#">News List</a></li>
            <li class=" depth-1"><a target="" href="#">News Detail</a></li>
            <li class=" depth-1"><a target="" href="#">Location Detail</a></li>
            <li class=" depth-1"><a target="" href="#">Become a vendor</a></li>
        </ul>
    </li>
    <li class=" depth-0"><a target="" href="/contact">Contact Us</a></li>
</ul> 
</div>
</div>
</div> 







@yield('body')









<div class="bravo_footer">
<div class="mailchimp">
<div class="container">
<div class="row">
<div class="col-xs-12 col-lg-10 col-lg-offset-1">
<div class="row">
<div class="col-xs-12  col-md-7 col-lg-6">
<div class="media ">
<div class="media-left hidden-xs">
<i class="icofont-island-alt"></i>
</div>
<div class="media-body">
<h4 class="media-heading">Get Updates &amp; More</h4>
<p>Thoughtful thoughts to your inbox</p>
</div>
</div>
</div>
<div class="col-xs-12 col-md-5 col-lg-6">
<form action="https://bookingcore.org/en/newsletter/subscribe" class="subcribe-form bravo-subscribe-form bravo-form">
<input type="hidden" name="_token" value="YcPobmrDSOSlAhzryYX0nxbq8TXzC4vlE7y5cWQE"> <div class="form-group">
<input type="text" name="email" class="form-control email-input" placeholder="Your Email">
<button type="submit" class="btn-submit">Subscribe
<i class="fa fa-spinner fa-pulse fa-fw"></i>
</button>
</div>
<div class="form-mess"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="main-footer">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-6">
<div class="nav-footer">
<div class="title">
NEED HELP?
</div>
<div class="context">
<div class="contact">
<div class="c-title">
Call Us
</div>
<div class="">
9933075171 / 9233377780
</div>
</div>
<div class="contact">
<div class="c-title">
Email for Us
</div>
<div class="">
<a hreh="">teletravels_haldia@yahoo.in</a>
</div>
</div>

</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="nav-footer">
    <div class="title">
    Address
    </div>
    <div class="context">
    <ul>
    <li><a href="#">Tele Travels</a></li>
    <li><a href="#">Mohana Super Market</a></li>
    <li><a href="#">Stall D/16. P.O. Haldia,Township.Pin-721607</a></li>
    <li><a href="#">Dist-Purbamedinipur. W.B</a></li>
    
    </ul>
    </div>
    </div>
    </div>
<div class="col-lg-3 col-md-6">
<div class="nav-footer">
<div class="title">
COMPANY
</div>
<div class="context">
<ul>
<li><a href="#">About Us</a></li>
<li><a href="#">News</a></li>
<li><a href="#">Gallery</a></li>
<li><a href="#">Contact Us</a></li>

</ul>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="nav-footer">
<div class="title">
Service
</div>
<div class="context">
 <ul>
<li><a href="#">Tour & Travel</a></li>
<li><a href="#">cab Service</a></li>
<li><a href="#">Mobile & Accessories</a></li>
<li><a href="#">Ticket Enquiry</a></li>

</ul>
</div>
</div>
</div>

</div>
</div>
</div>
<div class="copy-right">
<div class="container context">
<div class="row">
<div class="col-md-12">
Copyright by Teletravel 2019 ©  Designed and Developed by Quantex 
<div class="f-visa">
Teletravel
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade login" id="login" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content relative">
<div class="modal-header">
<h4 class="modal-title">Log In</h4>
<span class="c-pointer" data-dismiss="modal" aria-label="Close">
<i class="input-icon field-icon fa">
<img src="images\ico_close.svg" alt="close">
</i>
</span>
</div>
<div class="modal-body relative">
<form class="bravo-form-login" method="POST" action="https://bookingcore.org/en/login">
<input type="hidden" name="_token" value="YcPobmrDSOSlAhzryYX0nxbq8TXzC4vlE7y5cWQE"> <div class="form-group">
<input type="text" class="form-control" name="email" autocomplete="off" placeholder="Email address">
<i class="input-icon fa fa-envelope-o"></i>
<span class="invalid-feedback error error-email"></span>
</div>
<div class="form-group">
<input type="password" class="form-control" name="password" autocomplete="off" placeholder="Password">
<i class="input-icon fa fa-lock"></i>
<span class="invalid-feedback error error-password"></span>
</div>
<div class="form-group">
<div class="d-flex justify-content-between">
<label for="remember-me" class="mb0">
<input type="checkbox" name="remember" id="remember-me" value="1"> Remember me <span class="checkmark fcheckbox"></span>
</label>
<a href="#">Forgot Password?</a>
</div>
</div>
<div class="error message-error invalid-feedback"></div>
<div class="form-group">
<button class="btn btn-primary form-submit" type="submit">
Login
<span class="spinner-grow spinner-grow-sm icon-loading" role="status" aria-hidden="true"></span>
</button>
</div>
<div class="c-grey font-medium f14 text-center"> Do not have an account? <a href="" data-target="#register" data-toggle="modal">Sign Up</a>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="modal fade login" id="register" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content relative">
<div class="modal-header">
<h4 class="modal-title">Sign Up</h4>
<span class="c-pointer" data-dismiss="modal" aria-label="Close">
<i class="input-icon field-icon fa">
<img src="images\ico_close.svg" alt="close">
</i>
</span>
</div>
<div class="modal-body">
<form class="form bravo-form-register" method="post">
<input type="hidden" name="_token" value="YcPobmrDSOSlAhzryYX0nxbq8TXzC4vlE7y5cWQE"> <div class="row">
<div class="col-lg-6 col-md-12">
<div class="form-group">
<input type="text" class="form-control" name="first_name" autocomplete="off" placeholder="First Name">
<i class="input-icon field-icon fa"><img src="images\ico_fullname_signup.svg"></i>
<span class="invalid-feedback error error-first_name"></span>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="form-group">
<input type="text" class="form-control" name="last_name" autocomplete="off" placeholder="Last Name">
<i class="input-icon field-icon fa"><img src="images\ico_fullname_signup.svg"></i>
<span class="invalid-feedback error error-last_name"></span>
</div>
</div>
</div>
<div class="form-group">
<input type="email" class="form-control" name="email" autocomplete="off" placeholder="Email address">
<i class="input-icon field-icon fa"><img src="images\ico_email_login_form.svg"></i>
<span class="invalid-feedback error error-email"></span>
</div>
<div class="form-group">
<input type="password" class="form-control" name="password" autocomplete="off" placeholder="Password">
<i class="input-icon field-icon fa"><img src="images\ico_pass_login_form.svg"></i>
<span class="invalid-feedback error error-password"></span>
</div>
<div class="form-group">
<label for="term">
<input id="term" type="checkbox" name="term" class="mr5">
I have read and accept the <a href='' target='_blank'>Terms and Privacy Policy</a>
<span class="checkmark fcheckbox"></span>
</label>
<div><span class="invalid-feedback error error-term"></span></div>
</div>
<div class="error message-error invalid-feedback"></div>
<div class="form-group">
<button type="submit" class="btn btn-primary form-submit">
Sign Up
<span class="spinner-grow spinner-grow-sm icon-loading" role="status" aria-hidden="true"></span>
</button>
</div>
<div class="c-grey f14 text-center">
Already have an account?
<a href="#" data-target="#login" data-toggle="modal">Log In</a>
</div>
</form>
</div>
</div>
</div>
</div><link rel="stylesheet" href="libs\flags\css\flag-icon.min.css">
<script data-cfasync="false" src="cdn-cgi\scripts\5c5dd728\cloudflare-static\email-decode.min.js"></script><script src="libs\lazy-load\intersection-observer.js"></script>
<script async="" src="libs\lazy-load\lazyload.min.js"></script>
<script>
    // Set the options to make LazyLoad self-initialize
    window.lazyLoadOptions = {
        elements_selector: ".lazy",
        // ... more custom settings?
    };

    // Listen to the initialization event and get the instance of LazyLoad
    window.addEventListener('LazyLoad::Initialized', function (event) {
        window.lazyLoadInstance = event.detail.instance;
    }, false);


</script>
<script src="libs\lodash.min.js"></script>
<script src="libs\jquery-3.3.1.min.js"></script>
<script src="libs\vue\vue.js"></script>
<script src="libs\bootstrap\js\bootstrap.bundle.min.js"></script>
<script src="libs\bootbox\bootbox.min.js"></script>
<script src="libs\carousel-2\owl.carousel.min.js"></script>
<script type="text/javascript" src="libs\daterange\moment.min.js"></script>
<script type="text/javascript" src="libs\daterange\daterangepicker.min.js"></script>
<script src="libs\select2\js\select2.min.js"></script>
<script src="js\functions.js?_ver=1.5.1"></script>
<script src="js\home.js?_ver=1.5.1"></script>
<script type="text/javascript" src="libs\ion_rangeslider\js\ion.rangeSlider.min.js"></script>
<script type="text/javascript" src="module\car\js\car.js?_ver=1.5.1"></script>
<script src='https://maps.googleapis.com/maps/api/js?key=&libraries=places'></script><script src='libs\infobox.js'></script><script src='module\core\js\map-engine.js?_ver=1.5.1'></script></div>
<script>
    jQuery(function ($) {
                    new BravoMapEngine('map_content', {
            disableScripts: true,
            fitBounds: true,
            center: [48.852754, 2.339155],
            zoom:12,
            ready: function (engineMap) {
                engineMap.addMarker([48.852754, 2.339155], {
                    icon_options: {}
                });
            }
        });
                })
</script>
<script>
    var bravo_booking_data = {"id":1,"person_types":[{"name":"Adult","desc":"Age 18+","min":1,"max":10,"price":"1000","number":1,"display_price":"$1.000"},{"name":"Child","desc":"Age 6-17","min":0,"max":10,"price":"300","number":0,"display_price":"$300"}],"max":0,"open_hours":[],"extra_price":[{"name":"Clean","price":"100","type":"one_time","number":0,"enable":0,"price_html":"$100","price_type":""}],"minDate":"01\/20\/2020","duration":5,"buyer_fees":[{"name":"Service fee","desc":"This helps us run our platform and offer services like 24\/7 support on your trip.","name_ja":"\u30b5\u30fc\u30d3\u30b9\u6599","desc_ja":"\u3053\u308c\u306b\u3088\u308a\u3001\u5f53\u793e\u306e\u30d7\u30e9\u30c3\u30c8\u30d5\u30a9\u30fc\u30e0\u3092\u5b9f\u884c\u3057\u3001\u65c5\u884c\u4e2d\u306b","price":"100","type":"one_time","type_name":"Service fee","type_desc":"This helps us run our platform and offer services like 24\/7 support on your trip.","price_type":""}],"start_date":"","start_date_html":"","end_date":"","end_date_html":""}
    var bravo_booking_i18n = {
            no_date_select:'Please select Start date',
            no_guest_select:'Please select at least one guest',
            load_dates_url:'#'
        };
</script>


<script type="text/javascript" src="libs\ion_rangeslider\js\ion.rangeSlider.min.js"></script>
<script type="text/javascript" src="libs\fotorama\fotorama.js"></script>
<script type="text/javascript" src="libs\sticky\jquery.sticky.js"></script>
<script type="text/javascript" src="module\tour\js\single-tour.js?_ver=1.5.1"></script>

</body>
</html>
