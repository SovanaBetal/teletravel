@extends('layouts.master')
@section('body')

<div class="bravo-news">
<div class="bravo_banner" style="background-image: url(uploads/demo/news/news-banner.jpg)">
<div class="container">
<h1>
News
</h1>
</div>
</div>
<div class="blog-breadcrumb hidden-xs">
<div class="container">
<ul>
<li><a href="#"> Home</a></li>
<li class="active">
<a href="#">News</a>
</li>
</ul>
</div>
</div>
<div class="bravo_content">
<div class="container">
<div class="row">
<div class="col-md-9">
<div class="list-news">

    @foreach($cat as $cat)
<div class="post_item ">
<div class="header">
<header class="post-header">
 <a href="/news-details?id={{$cat->id}}">
 <img class=' lazy' data-src="upload/{{$cat->image}}" alt=''>
</a>
</header>
<div class="cate">
<ul>
<li>
<a href="/news-details?id={{$cat->id}}">
    {{$cat->title}}
</a>
</li>
</ul>
</div>
<div class="post-inner">
<h4 class="post-title">
<a class="text-darken" href="/news-details?id={{$cat->id}}">{{$cat->subtitle}}</a>
</h4>
<div class="post-info">
<ul>
<li>
<img class="avatar" src="uploads\0000\1\2019\11\15\portfolio7-350x400-150.jpg" alt="System Admin">


</li>
<li> DATE {{$cat->created_at}}</li>
</ul>
</div>
<div class="post-desciption">


      @php($value=$cat->description) 
      @php($result=explode('.',$value))


{{$result[0]}} [......]
   
</div>
<a class="btn-readmore" href="/news-details?id={{$cat->id}}">Read More</a>
</div>
</div>
</div>

@endforeach




<hr>
<!--<div class="bravo-pagination">
<ul class="pagination" role="navigation">
<li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Previous">
<span class="page-link" aria-hidden="true">&lsaquo;</span>
</li>
<li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
<li class="page-item"><a class="page-link" href="news-2.html?page=2">2</a></li>
<li class="page-item">
<a class="page-link" href="news-2.html?page=2" rel="next" aria-label="Next &raquo;">&rsaquo;</a>
</li>
</ul>
<span class="count-string">Showing 1 - 5 of 8 posts</span>
</div>-->
</div>
</div>
<div class="col-md-3">
<aside class="sidebar-right">
<div class="sidebar-widget widget_search">
<form method="get" class="search" action="https://bookingcore.org/en/news">
<input type="text" class="form-control" value="" name="s" placeholder="Search ...">
<button type="submit" class="icon_search"></button>
</form>
</div> <div class="sidebar-widget">
<div class="sidebar-title">
<h4>About Us</h4>
</div>
<div class="textwidget">
Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus, quis mollis nisl nunc et massa
</div>
</div> <div class="sidebar-widget widget_bloglist">
<div class="sidebar-title">
<h4>Recent News</h4>
</div>
<ul class="thumb-list">
    @php($cat=DB::table('cats')->limit(5)->get())
    @foreach($cat as $cat)
<li>
<div class="thumb">
<a href="/news-details?id={{$cat->id}}">
<img class=' lazy' data-src="upload/{{$cat->image}}" alt='Morning in the Northern sea'>
</a>
</div>
<div class="content">
<div class="cate">
<a href="/news-details?id={{$cat->id}}">
    {{$cat->title}}
</a>
</div>
<h5 class="thumb-list-item-title">
<a href="/news-details?id={{$cat->id}}">{{$cat->subtitle}}</a>
</h5>
</div>
</li>
@endforeach




</ul>
</div>

<div class="sidebar-widget widget_tag_cloud">

<div class="tagcloud">
<ul>
</ul>
</div>
</div>
</aside> </div>
</div>
</div>
</div>
</div>
@endsection