﻿@extends('layouts.master')
@section('body')

<div class="bravo_detail_location">
<div class="bravo_banner" style="background-image: linear-gradient(0deg,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.2)),url('uploads/demo/location/banner-detail/banner-location-1.jpg') !important"></div>
<div class="bravo_content">
<div class="container">
<div class="row py-5">
<div class="col-md-12 col-lg-8">
<h3>About Us</h3>
<div class="g-overview">
<div class="description">
New York, a city that doesnt sleep, as Frank Sinatra sang. The Big Apple is one of the largest cities in the United States and one of the most popular in the whole country and the world. Millions of tourists visit it every year attracted by its various iconic symbols and its wide range of leisure and cultural offer. New York is the birth place of new trends and developments in many fields such as art, gastronomy, technology,... </div>
</div>
</div>
<div class="col-md-12 col-lg-4">
<div class="g-thumbnail m-3">
<img data-src="https://bookingcore.org/uploads/demo/location/location-1.jpg" class="img-fluid lazy" alt="">
</div>
</div>
</div>
<div class="g-location-module py-5 border-top border-bottom">

<div class="tab-content clearfix py-5">


<div class="tab-pane " id="module-space">
<div class="container">
<div class="bravo-list-space layout_normal">
<div class="list-item">
<div class="row">
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\duplex-greenwich.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-5.jpg" alt='DUPLEX GREENWICH'>
</a>


</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="featured">
Featured
</div>
<div class="thumb-image ">
<a href="en\space\the-meatpacking-suites.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-6.jpg" alt='THE MEATPACKING SUITES'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale"></span>
<span class="text-price">34.892 ¥ <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="6" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\the-meatpacking-suites.html">
<i class="fa fa-bolt d-none"></i>
THE MEATPACKING SUITES
</a>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.7/5 <span class="rate-text">Excellent</span>
</span>
<span class="review">
3 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 6
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 4
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 6
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\east-village.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-7.jpg" alt='EAST VILLAGE'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale">32.711 ¥</span>
<span class="text-price">28.350 ¥ <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="7" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\east-village.html">
EAST VILLAGE
</a>
<div class="sale_info">13%</div>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.0/5 <span class="rate-text">Very Good</span>
</span>
<span class="review">
2 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 7
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 7
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 7
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\paris-greenwich-villa.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-8.jpg" alt='PARIS GREENWICH VILLA'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale"></span>
<span class="text-price">54.519 ¥ <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="8" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\paris-greenwich-villa.html">
<i class="fa fa-bolt d-none"></i>
PARIS GREENWICH VILLA
</a>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.5/5 <span class="rate-text">Excellent</span>
</span>
<span class="review">
4 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 7
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 9
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 1
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="featured">
Featured
</div>
<div class="thumb-image ">
<a href="en\space\luxury-single.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-9.jpg" alt='LUXURY SINGLE'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale">43.615 ¥</span>
<span class="text-price">38.163 ¥ <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="9" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\luxury-single.html">
<i class="fa fa-bolt d-none"></i>
LUXURY SINGLE
</a>
<div class="sale_info">12%</div>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.5/5 <span class="rate-text">Excellent</span>
</span>
<span class="review">
2 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 6
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 9
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 8
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\lily-dale-village.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-10.jpg" alt='LILY DALE VILLAGE'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale"></span>
<span class="text-price">27.259 ¥ <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="10" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\lily-dale-village.html">
LILY DALE VILLAGE
</a>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.0/5 <span class="rate-text">Very Good</span>
</span>
<span class="review">
2 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 6
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 4
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 9
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 101 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\stay-greenwich-village.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-11.jpg" alt='STAY GREENWICH VILLAGE'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale">32.711 ¥</span>
<span class="text-price">16.356 ¥ <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="11" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\stay-greenwich-village.html">
STAY GREENWICH VILLAGE
</a>
<div class="sale_info">50%</div>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.8/5 <span class="rate-text">Excellent</span>
</span>
<span class="review">
5 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 8
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 5
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 10
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div> <p class="text-center"><a class="btn btn-primary btn-search" href="en\space.html?location_id=1">View More</a></p>
</div>
<div class="tab-pane " id="module-tour">
<div class="bravo-list-tour normal">

</div> <p class="text-center"><a class="btn btn-primary btn-search" href="en\tour.html?location_id=1">View More</a></p>
</div>
</div>
</div>
<div class="row">
<div class="col-12">
<h3 class="py-5">The City Maps</h3>
</div>
</div>
</div>
<div class="g-location">
<div class="location-map">
<div id="map_content"></div>
</div>
</div>


</div>
</div>
@endsection