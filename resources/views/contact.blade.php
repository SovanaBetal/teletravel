﻿@extends('layouts.master')
@section('body')

<div id="bravo_content-wrapper">
<div class="bravo_content">
<div class="container">
<div class="row section">
<div class="col-md-12 col-lg-5">
<div role="form" class="form_wrapper" lang="en-US" dir="ltr">
<form method="post" action="https://bookingcore.org/en/contact/store" class="bookcore-form">
<input type="hidden" name="_token" value="YcPobmrDSOSlAhzryYX0nxbq8TXzC4vlE7y5cWQE">
<div style="display: none;">
<input type="hidden" name="g-recaptcha-response" value="">
</div>
<div class="contact-form">
<div class="contact-header">
<h3>We&#039;d love to hear from you</h3>
<p>Send us a message and we&#039;ll respond as soon as possible</p>
</div>
<div class="contact-form">
<div class="form-group">
<input type="text" value="" placeholder=" Name " name="name" class="form-control">
</div>
<div class="form-group">
<input type="text" value="" placeholder="Email" name="email" class="form-control">
</div>
<div class="form-group">
<textarea name="message" cols="40" rows="10" class="form-control textarea" placeholder="Message"></textarea>
 </div>
<div class="form-group">
</div>
<p><input type="submit" value="SEND MESSAGE" class="form-control submit btn btn-primary"></p></div></div>
</form>
</div>
</div>
<div class="offset-lg-2 col-md-12 col-lg-5">
<div class="contact-info">
<div class="info-bg">
<img src="uploads\demo\general\bg-contact.jpg" class="img-responsive" alt="We&#039;d love to hear from you">
</div>
<div class="info-content">
<div class="sub">
<p><!DOCTYPE html><html><head></head><body><h3>Booking Core</h3><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>Tell. + 00 222 444 33</p><p>Email. <a href="cdn-cgi\l\email-protection.html" class="__cf_email__" data-cfemail="f79f929b9b98b78e988285849e8392d994989a">[email&#160;protected]</a></p><p>1355 Market St, Suite 900San, Francisco, CA 94103 United States</p><script data-cfasync="false" src="cdn-cgi\scripts\5c5dd728\cloudflare-static\email-decode.min.js"></script></body></html></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection