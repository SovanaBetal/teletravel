﻿@extends('layouts.master')
@section('body')
@foreach($travel as $travel)
<div class="bravo_detail_tour">
<div class="bravo_banner" style="background-image: url('upload/')">
<div class="container">
 <div class="bravo_gallery">
<div class="btn-group">
<a href="#" class="btn btn-transparent has-icon bravo-video-popup" data-toggle="modal" data-src="https://www.youtube.com/embed/UfEiKK-iX70" data-target="#myModal">
<i class="input-icon field-icon fa">
<svg height="18px" width="18px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
<g fill="#FFFFFF">
<path d="M2.25,24C1.009,24,0,22.991,0,21.75V2.25C0,1.009,1.009,0,2.25,0h19.5C22.991,0,24,1.009,24,2.25v19.5
                                            c0,1.241-1.009,2.25-2.25,2.25H2.25z M2.25,1.5C1.836,1.5,1.5,1.836,1.5,2.25v19.5c0,0.414,0.336,0.75,0.75,0.75h19.5
                                            c0.414,0,0.75-0.336,0.75-0.75V2.25c0-0.414-0.336-0.75-0.75-0.75H2.25z">
</path>
<path d="M9.857,16.5c-0.173,0-0.345-0.028-0.511-0.084C8.94,16.281,8.61,15.994,8.419,15.61c-0.11-0.221-0.169-0.469-0.169-0.716
                                            V9.106C8.25,8.22,8.97,7.5,9.856,7.5c0.247,0,0.495,0.058,0.716,0.169l5.79,2.896c0.792,0.395,1.114,1.361,0.719,2.153
                                            c-0.154,0.309-0.41,0.565-0.719,0.719l-5.788,2.895C10.348,16.443,10.107,16.5,9.857,16.5z M9.856,9C9.798,9,9.75,9.047,9.75,9.106
                                            v5.788c0,0.016,0.004,0.033,0.011,0.047c0.013,0.027,0.034,0.044,0.061,0.054C9.834,14.998,9.845,15,9.856,15
                                            c0.016,0,0.032-0.004,0.047-0.011l5.788-2.895c0.02-0.01,0.038-0.027,0.047-0.047c0.026-0.052,0.005-0.115-0.047-0.141l-5.79-2.895
                                            C9.889,9.004,9.872,9,9.856,9z">
</path>
</g>
</svg>
</i>Tour Video
</a>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item bravo_embed_video" src="" allowscriptaccess="always" allow="autoplay"></iframe>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="bravo_content">
<div class="container">
<div class="row">
<div class="col-md-12 col-lg-9">
<div class="g-header">
<div class="left">
<h2>American Parks Trail end Rapid City</h2>
<p class="address"><i class="fa fa-map-marker"></i>
Arrondissement de Paris
</p>
</div>
<div class="right">
<div class="review-score">
<div class="head">
<div class="left">
<span class="head-rating">Very Good</span>
<span class="text-rating">from 4 reviews</span>
</div>
<div class="score">
4.3<span>/5</span>
</div>
</div>
<div class="foot">
100% of guests recommend
</div>
</div>
</div>
</div>
<div class="g-tour-feature">
<div class="row">
<div class="col-xs-6 col-lg-3 col-md-6">
<div class="item">
<div class="icon">
<i class="icofont-wall-clock"></i>
</div>
<div class="info">
<h4 class="name">Duration</h4>
<p class="value">
5 hours
</p>
</div>
</div>
</div>
<div class="col-xs-6 col-lg-3 col-md-6">
<div class="item">
<div class="icon">
<i class="icofont-beach"></i>
</div>
<div class="info">
<h4 class="name">Tour Type</h4>
<p class="value">
Escorted tour
</p>
</div>
</div>
</div>
<div class="col-xs-6 col-lg-3 col-md-6">
<div class="item">
<div class="icon">
<i class="icofont-travelling"></i>
</div>
<div class="info">
<h4 class="name">Group Size</h4>
<p class="value">
20 persons
</p>
</div>
</div>
</div>
<div class="col-xs-6 col-lg-3 col-md-6">
<div class="item">
<div class="icon">
<i class="icofont-island-alt"></i>
</div>
<div class="info">
<h4 class="name">Location</h4>
<p class="value">
Paris
</p>
</div>
</div>
</div>
</div>
</div>
<div class="g-gallery">
<div class="fotorama" data-width="100%" data-thumbwidth="135" data-thumbheight="135" data-thumbmargin="15" data-nav="thumbs" data-allowfullscreen="true">

    @php($id=$travel->id)
    @php($travelimage=DB::table('travelimages')->where('travel_id',$id)->get())
    @foreach($travelimage as $travelimage)

<a href="upload/{{$travelimage->image}}" data-thumb="upload/{{$travelimage->image}}"></a>

    @endforeach
<!--<a href="uploads\demo\tour\gallery-2.jpg" data-thumb="uploads/demo/tour/gallery-2.jpg"></a>
<a href="uploads\demo\tour\gallery-3.jpg" data-thumb="uploads/demo/tour/gallery-3.jpg"></a>
<a href="uploads\demo\tour\gallery-4.jpg" data-thumb="uploads/demo/tour/gallery-4.jpg"></a>
<a href="uploads\demo\tour\gallery-5.jpg" data-thumb="uploads/demo/tour/gallery-5.jpg"></a>
<a href="uploads\demo\tour\gallery-6.jpg" data-thumb="uploads/demo/tour/gallery-6.jpg"></a>
<a href="uploads\demo\tour\gallery-7.jpg" data-thumb="uploads/demo/tour/gallery-7.jpg"></a>-->
</div>
<div class="social">
<div class="social-share">
<span class="social-icon">
<i class="icofont-share"></i>
</span>
<ul class="share-wrapper">
<li>
 <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=https://bookingcore.org/en/tour/american-parks-trail&title=American Parks Trail end Rapid City" target="_blank" rel="noopener" original-title="Facebook">
<i class="fa fa-facebook fa-lg"></i>
</a>
</li>
<li>
<a class="twitter" href="https://twitter.com/share?url=https://bookingcore.org/en/tour/american-parks-trail&title=American Parks Trail end Rapid City" target="_blank" rel="noopener" original-title="Twitter">
<i class="fa fa-twitter fa-lg"></i>
</a>
</li>
</ul>
</div>
<div class="service-wishlist " data-id="1" data-type="tour">
<i class="fa fa-heart-o"></i>
</div>
</div>
</div>
<div class="g-overview">
<h3>Overview</h3>
<div class="description">
<p> {{$travel->description}}.</p>
<!--<h4>HIGHLIGHTS</h4>
    <ul>
     <li>Visit the Museum of Modern Art in Manhattan</li>
    <li>See amazing works of contemporary art, including Vincent van Gogh's The Starry Night</li>
    <li>Check out Campbell's Soup Cans by Warhol and The Dance (I) by Matisse</li>
    <li>Behold masterpieces by Gauguin, Dali, Picasso, and Pollock</li>
    <li>Enjoy free audio guides available in English, French, German, Italian, Spanish, Portuguese</li>
</ul> -->
</div>
</div>
<!--<div class="g-include-exclude">
<h3> Included/Excluded </h3>
<div class="row">
<div class="col-lg-6 col-md-6">
<div class="item">
<i class="icofont-check-alt icon-include"></i>
Specialized bilingual guide
</div>
<div class="item">
<i class="icofont-check-alt icon-include"></i>
Private Transport
</div>
<div class="item">
<i class="icofont-check-alt icon-include"></i>
Entrance fees (Cable and car and Moon Valley)
</div>
<div class="item">
<i class="icofont-check-alt icon-include"></i>
Box lunch water, banana apple and chocolate
</div>
</div>
<div class="col-lg-6 col-md-6">
<div class="item">
<i class="icofont-close-line icon-exclude"></i>
Additional Services
</div>
<div class="item">
<i class="icofont-close-line icon-exclude"></i>
Insurance
</div>
<div class="item">
<i class="icofont-close-line icon-exclude"></i>
Drink
</div>
<div class="item">
<i class="icofont-close-line icon-exclude"></i>
Tickets
</div>
</div>
</div>
</div>
<div class="g-itinerary">
<h3> Itinerary </h3>
<div class="list-item owl-carousel">
<div class="item" style="background-image: url('uploads/demo/location/location-5.jpg')">
<div class="header">
<div class="item-title">Day 1</div>
<div class="item-desc">Los Angeles</div>
</div>
<div class="body">
<div class="item-title">Day 1</div>
<div class="item-desc">Los Angeles</div>
<div class="item-context">
There are no activities planned until an evening welcome meeting. Additional Notes: We highly recommend booking pre-tour accommodation to fully experience this crazy city.
</div>
</div>
</div>
<div class="item" style="background-image: url('uploads/demo/location/location-4.jpg')">
<div class="header">
<div class="item-title">Day 2</div>
<div class="item-desc">Lake Havasu City</div>
</div>
<div class="body">
<div class="item-title">Day 2</div>
<div class="item-desc">Lake Havasu City</div>
<div class="item-context">
Pack up the van in the morning and check out the stars on the most famous sidewalk in Hollywood on an orientation tour
</div>
</div>
</div>
<div class="item" style="background-image: url('uploads/demo/location/location-3.jpg')">
<div class="header">
<div class="item-title">Day 3</div>
<div class="item-desc">Las Vegas/Bakersfield</div>
</div>
<div class="body">
<div class="item-title">Day 3</div>
<div class="item-desc">Las Vegas/Bakersfield</div>
<div class="item-context">
Travel to one of the country's most rugged landscapes — the legendary Death Valley, California. Soak in the dramatic landscape. In the afternoon, continue on to Bakersfield for the night.
</div>
</div>
</div>
<div class="item" style="background-image: url('uploads/demo/location/location-2.jpg')">
<div class="header">
<div class="item-title">Day 4</div>
<div class="item-desc">San Francisco</div>
</div>
<div class="body">
<div class="item-title">Day 4</div>
<div class="item-desc">San Francisco</div>
<div class="item-context">
We highly recommend booking post-accommodation to fully experience this famous city.
</div>
</div>
</div>
</div>
</div>-->
<!--<div class="g-attributes travel-styles attr-1">
<h3>Travel Styles</h3>
<div class="list-attributes">
<div class="item cultural term-1"><i class="icofont-check-circled"></i> Cultural</div>
<div class="item nature-adventure term-2"><i class="icofont-check-circled"></i> Nature &amp; Adventure</div>
<div class="item marine term-3"><i class="icofont-check-circled"></i> Marine</div>
<div class="item independent term-4"><i class="icofont-check-circled"></i> Independent</div>
<div class="item activities term-5"><i class="icofont-check-circled"></i> Activities</div>
 <div class="item festival-events term-6"><i class="icofont-check-circled"></i> Festival &amp; Events</div>
<div class="item special-interest term-7"><i class="icofont-check-circled"></i> Special Interest</div>
</div>
</div>
<div class="g-attributes facilities attr-2">
<h3>Facilities</h3>
<div class="list-attributes">
<div class="item wifi term-8"><i class="icofont-check-circled"></i> Wifi</div>
<div class="item gymnasium term-9"><i class="icofont-check-circled"></i> Gymnasium</div>
<div class="item aerobics-room term-14"><i class="icofont-check-circled"></i> Aerobics Room</div>
</div>
</div>
<div class="g-faq">
<h3> FAQs </h3>
<div class="item">
<div class="header">
<i class="field-icon icofont-support-faq"></i>
<h5>When and where does the tour end?</h5>
<span class="arrow"><i class="fa fa-angle-down"></i></span>
</div>
<div class="body">
Your tour will conclude in San Francisco on Day 8 of the trip. There are no activities planned for this day so you're free to depart at any time. We highly recommend booking post-accommodation to give yourself time to fully experience the wonders of this iconic city!
</div>
</div>
<div class="item">
<div class="header">
<i class="field-icon icofont-support-faq"></i>
<h5>When and where does the tour start?</h5>
<span class="arrow"><i class="fa fa-angle-down"></i></span>
</div>
<div class="body">
Day 1 of this tour is an arrivals day, which gives you a chance to settle into your hotel and explore Los Angeles. The only planned activity for this day is an evening welcome meeting at 7pm, where you can get to know your guides and fellow travellers. Please be aware that the meeting point is subject to change until your final documents are released.
</div>
</div>
<div class="item">
<div class="header">
<i class="field-icon icofont-support-faq"></i>
<h5>Do you arrange airport transfers?</h5>
<span class="arrow"><i class="fa fa-angle-down"></i></span>
</div>
<div class="body">
Airport transfers are not included in the price of this tour, however you can book for an arrival transfer in advance. In this case a tour operator representative will be at the airport to greet you. To arrange this please contact our customer service team once you have a confirmed booking.
</div>
</div>
<div class="item">
<div class="header">
<i class="field-icon icofont-support-faq"></i>
<h5>What is the age range</h5>
<span class="arrow"><i class="fa fa-angle-down"></i></span>
</div>
<div class="body">
This tour has an age range of 12-70 years old, this means children under the age of 12 will not be eligible to participate in this tour. However, if you are over 70 years please contact us as you may be eligible to join the tour if you fill out G Adventures self-assessment form.
</div>
</div>
</div>
<div class="g-location">
<div class="location-title">
<h3>Tour Location</h3>
<div class="address">
<i class="icofont-location-arrow"></i>
Arrondissement de Paris
</div>
</div>
<div class="location-map">
<div id="map_content"></div>
</div>
</div>
<div class="bravo-reviews" id="bravo-reviews">
<h3>Reviews</h3>
<div class="review-box">
<div class="row">
<div class="col-lg-5">
<div class="review-box-score">
<div class="review-score">
4.3<span class="per-total">/5</span>
</div>
<div class="review-score-text">
Very Good
</div>
<div class="review-score-base">
Based on
<span>
4 reviews
</span>
</div>
</div>
</div>
<div class="col-lg-7">
<div class="review-sumary">
<div class="item">
<div class="label">
Excellent
</div>
<div class="progress">
<div class="percent green" style="width: 25%"></div>
</div>
<div class="number">1</div>
</div>
<div class="item">
<div class="label">
Very Good
</div>
<div class="progress">
<div class="percent green" style="width: 75%"></div>
</div>
<div class="number">3</div>
</div>
<div class="item">
<div class="label">
Average
</div>
<div class="progress">
<div class="percent green" style="width: 0%"></div>
</div>
<div class="number">0</div>
</div>
<div class="item">
<div class="label">
Poor
</div>
<div class="progress">
<div class="percent green" style="width: 0%"></div>
</div>
<div class="number">0</div>
</div>
<div class="item">
<div class="label">
Terrible
</div>
<div class="progress">
<div class="percent green" style="width: 0%"></div>
</div>
<div class="number">0</div>
</div>
 </div>
</div>
</div>
</div>
<div class="review-list">
<div class="review-item">
<div class="review-item-head">
<div class="media">
<div class="media-left">
<span class="avatar-text">E</span>
</div>
<div class="media-body">
<h4 class="media-heading">Ed Fonx</h4>
<div class="date">01/18/2020 19:41</div>
</div>
</div>
</div>
<div class="review-item-body">
<h4 class="title"> Excelent </h4>
<ul class="review-star">
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
</ul>
<div class="detail">
Best service
</div>
</div>
</div>
<div class="review-item">
<div class="review-item-head">
<div class="media">
<div class="media-left">
<span class="avatar-text">C</span>
</div>
<div class="media-body">
<h4 class="media-heading">customer asd</h4>
<div class="date">12/12/2019 11:54</div>
</div>
</div>
</div>
<div class="review-item-body">
<h4 class="title"> sadf </h4>
<ul class="review-star">
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
 <li><i class="fa fa-star-o"></i></li>
</ul>
<div class="detail">
asdf asd sd fsd f
</div>
</div>
</div>
<div class="review-item">
<div class="review-item-head">
<div class="media">
<div class="media-left">
<span class="avatar-text">S</span>
</div>
<div class="media-body">
<h4 class="media-heading">Shushi Yashashree</h4>
<div class="date">11/14/2019 10:25</div>
</div>
</div>
</div>
<div class="review-item-body">
<h4 class="title"> Easy way to discover the city </h4>
<ul class="review-star">
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star-o"></i></li>
</ul>
<div class="detail">
Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te
</div>
</div>
</div>
<div class="review-item">
<div class="review-item-head">
<div class="media">
<div class="media-left">
<span class="avatar-text">B</span>
</div>
<div class="media-body">
<h4 class="media-heading">Bush Elise</h4>
<div class="date">11/14/2019 10:25</div>
</div>
</div>
</div>
<div class="review-item-body">
<h4 class="title"> Easy way to discover the city </h4>
<ul class="review-star">
<li><i class="fa fa-star"></i></li>
 <li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star-o"></i></li>
</ul>
<div class="detail">
Eum eu sumo albucius perfecto, commodo torquatos consequuntur pro ut, id posse splendide ius. Cu nisl putent omittantur usu, mutat atomorum ex pro, ius nibh nonumy id. Nam at eius dissentias disputando, molestie mnesarchum complectitur per te
</div>
</div>
</div>
</div>
<div class="review-pag-wrapper">
<div class="bravo-pagination">
</div>
<div class="review-pag-text">
Showing 1 - 4 of 4 total
</div>
</div>
<div class="review-message">
You must <a href='#login' data-toggle='modal' data-target='#login'>log in</a> to write review
</div>
</div>-->
</div>
<div class="col-md-12 col-lg-3">
<div class="owner-info widget-box">
<div class="media">
<div class="media-left">
<a href="en\profile\1.html" target="_blank">
<img class="avatar avatar-96 photo origin round" src="uploads\0000\1\2019\11\15\portfolio7-350x400-150.jpg" alt="System Admin">
</a>
</div>
<div class="media-body">
<h4 class="media-heading"><a class="author-link" href="en\profile\1.html" target="_blank">System Admin</a>
<img data-toggle="tooltip" data-placement="top" src="icon\ico-vefified-1.svg" title="Verified" alt="ico-vefified-1">
</h4>
<p>Member Since Nov 2019</p>
</div>
</div>
</div>
<div class="bravo_single_book_wrap ">
<div class="bravo_single_book">
<div id="bravo_tour_book_app" v-cloak="">
<div class="tour-sale-box">
<span class="sale_class box_sale sale_small">68%</span>
</div>
<div class="form-head">
<div class="price">
<span class="label">
from
</span>
<span class="value">
<span class="onsale">$2.000</span>
<span class="text-lg">$637</span>
</span>
</div>
</div>
<div class="form-content">
<div class="form-group form-date-field form-date-search clearfix " data-format="MM/DD/YYYY">
<div class="date-wrapper clearfix" @click="openStartDate">
<div class="check-in-wrapper">
<label>Start Date</label>
<div class="render check-in-render"></div>
</div>
<i class="fa fa-angle-down arrow"></i>
</div>
<input type="text" class="start_date" ref="start_date" style="height: 1px; visibility: hidden">
</div>
<div class="" v-if="person_types">
<div class="form-group form-guest-search" v-for="(type,index) in person_types">
<div class="guest-wrapper d-flex justify-content-between align-items-center">
<div class="flex-grow-1">
<label></label>
<div class="render check-in-render"></div>
<div class="render check-in-render"> per person</div>
</div>
<div class="flex-shrink-0">
<div class="input-number-group">
<i class="icon ion-ios-remove-circle-outline" @click="minusPersonType(type)"></i>
<span class="input"></span>
<i class="icon ion-ios-add-circle-outline" @click="addPersonType(type)"></i>
</div>
</div>
</div>
</div>
</div>
<div class="form-group form-guest-search" v-else="">
<div class="guest-wrapper d-flex justify-content-between align-items-center">
<div class="flex-grow-1">
<label>Guests</label>
</div>
<div class="flex-shrink-0">
<div class="input-number-group">
<i class="icon ion-ios-remove-circle-outline" @click="minusGuestsType()"></i>
<span class="input"></span>
<i class="icon ion-ios-add-circle-outline" @click="addGuestsType()"></i>
</div>
</div>
</div>
</div>
<div class="form-section-group form-group" v-if="extra_price.length">
<h4 class="form-section-title">Extra prices:</h4>
<div class="form-group" v-for="(type,index) in extra_price">
<div class="extra-price-wrap d-flex justify-content-between">
<div class="flex-grow-1">
<label><input type="checkbox" v-model="type.enable"> </label>
<div class="render" v-if="type.price_type"></div>
</div>
<div class="flex-shrink-0"></div>
</div>
</div>
</div>
<div class="form-section-group form-group-padding" v-if="buyer_fees.length">
<div class="extra-price-wrap d-flex justify-content-between" v-for="(type,index) in buyer_fees">
<div class="flex-grow-1">
<label>
<i class="icofont-info-circle" v-if="type.desc" data-toggle="tooltip" data-placement="top" :title="type.type_desc"></i>
</label>
<div class="render" v-if="type.price_type"></div>
</div>
<div class="flex-shrink-0">
</div>
</div>
</div>
</div>
<div class="form-section-total" v-if="total_price > 0">
<label>Total</label>
<span class="price"></span>
</div>
<div v-html="html"></div>
<div class="submit-group">
<a class="btn btn-large" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'btn-primary':step == 1}" name="submit">
<span v-if="step == 1">BOOK NOW</span>
<span v-if="step == 2">Book Now</span>
<i v-show="onSubmit" class="fa fa-spinner fa-spin"></i>
</a>
<div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row end_tour_sticky">






</div>
</div>
</div>
<div class="bravo-more-book-mobile">
<div class="container">
<div class="left">
<div class="g-price">
<div class="prefix">
<span class="fr_text">from</span>
</div>
<div class="price">
<span class="onsale">$2.000</span>
<span class="text-price">$637</span>
</div>
</div>
<div class="service-review tour-review-4.3">
<div class="list-star">
<ul class="booking-item-rating-stars">
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
</ul>
<div class="booking-item-rating-stars-active" style="width: 86%">
<ul class="booking-item-rating-stars">
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
</ul>
</div>
</div>
<span class="review">
4 Reviews
</span>
</div>
</div>
<div class="right">
<a class="btn btn-primary bravo-button-book-mobile">Book Now</a>
</div>
</div>
</div>
</div>
@endforeach
@endsection