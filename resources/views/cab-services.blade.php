@extends('layouts.master')
@section('body')

<div class="bravo_search_space">
<div class="bravo_banner" style="background-image: url(uploads/demo/space/banner-search-space-2.jpg)">
<div class="container">
<h1>
Search for car
</h1>
</div>
</div>
<div class="bravo_form_search">
<div class="container">
<div class="row">
 <div class="col-lg-12 col-md-12">
<form action="https://bookingcore.org/en/car" class="form bravo_form" method="get">
<div class="g-field-search">
<div class="row">
<div class="col-md-6 border-right">
<div class="form-group">
<i class="field-icon fa icofont-map"></i>
<div class="form-content">
<label>Location</label>
<div class="smart-search">
<input type="text" class="smart-search-location parent_text form-control" readonly="" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
<input type="hidden" class="child_id" name="location_id" value="">
</div>
</div>
</div>
</div>
<div class="col-md-6 border-right">
<div class="form-group">
<i class="field-icon icofont-wall-clock"></i>
<div class="form-content">
<div class="form-date-search">
<div class="date-wrapper">
<div class="check-in-wrapper">
<label>From - To</label>
<div class="render check-in-render">01/20/2020</div>
<span> - </span>
<div class="render check-out-render">01/21/2020</div>
</div>
</div>
<input type="hidden" class="check-in-input" value="01/20/2020" name="start">
<input type="hidden" class="check-out-input" value="01/21/2020" name="end">
<input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
</div>
</div>
</div>
</div>
</div>
</div>
<div class="g-button-submit">
<button class="btn btn-primary btn-search" type="submit">Search</button>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">

<div class="col-lg-12 col-md-12">
<div class="bravo-list-item">
<div class="topbar-search">

<div class="control">
</div>
</div>
<div class="list-item">
<div class="row">



    @php($car=DB::table('cars')->get())
@foreach($car as $car)

<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a target="_blank" href="/cab-services-details?id={{$car->id}}">

    @php($id=$car->id)
    @php($carimage=DB::table('carimages')->where('car_id',$id)->limit(1)->get())
    @foreach($carimage as $carimage)
<img class='img-responsive lazy' data-src="upload/{{$carimage->image}}" alt='Vinfast Fadil Standard'>
    @endforeach
</a>
<div class="service-wishlist " data-id="13" data-type="car">
<i class="fa fa-heart-o"></i>
</div>
</div>
<div class="item-title">
<a target="_blank" href="/cab-services-details?id={{$car->id}}">
<i class="fa fa-bolt d-none"></i>
{{$car->title}}
</a>
</div>

<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="Passenger">
<i class="input-icon field-icon icon-passenger  "></i>
<span class="text">
    {{$car->guest}}
</span>
</span>

<span class="onsale"></span>
<span class="text-price">Rs. {{$car->price}}<span class="unit">/night</span></span>

</div>
<div class="info">

</div>
</div>
</div>

@endforeach




</div>
</div>
<!--<div class="bravo-pagination">
<ul class="pagination" role="navigation">
<li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Previous">
<span class="page-link" aria-hidden="true">&lsaquo;</span>
</li>
<li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
<li class="page-item"><a class="page-link" href="car-3.html?page=2">2</a></li>
<li class="page-item">
<a class="page-link" href="car-3.html?page=2" rel="next" aria-label="Next &raquo;">&rsaquo;</a>
</li>
</ul>
<span class="count-string">Showing 1 - 9 of 13 Cars</span>
</div>-->
</div>
</div>
</div>
</div>
</div>

@endsection