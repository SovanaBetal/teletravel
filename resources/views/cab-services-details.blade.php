
@extends('layouts.master')
@section('body')
@foreach($car as $car)
<div class="bravo_detail_space">
<div class="bravo_banner" style="background-image: url('uploads/demo/car/banner-single.jpg')">
<div class="container">
<div class="bravo_gallery">
<div class="btn-group">
<a href="#" class="btn btn-transparent has-icon bravo-video-popup" data-toggle="modal" data-src="https://www.youtube.com/embed/UfEiKK-iX70" data-target="#myModal">
<i class="input-icon field-icon fa">
<svg height="18px" width="18px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
<g fill="#FFFFFF">
<path d="M2.25,24C1.009,24,0,22.991,0,21.75V2.25C0,1.009,1.009,0,2.25,0h19.5C22.991,0,24,1.009,24,2.25v19.5
                                            c0,1.241-1.009,2.25-2.25,2.25H2.25z M2.25,1.5C1.836,1.5,1.5,1.836,1.5,2.25v19.5c0,0.414,0.336,0.75,0.75,0.75h19.5
                                            c0.414,0,0.75-0.336,0.75-0.75V2.25c0-0.414-0.336-0.75-0.75-0.75H2.25z">
</path>
<path d="M9.857,16.5c-0.173,0-0.345-0.028-0.511-0.084C8.94,16.281,8.61,15.994,8.419,15.61c-0.11-0.221-0.169-0.469-0.169-0.716
                                            V9.106C8.25,8.22,8.97,7.5,9.856,7.5c0.247,0,0.495,0.058,0.716,0.169l5.79,2.896c0.792,0.395,1.114,1.361,0.719,2.153
                                            c-0.154,0.309-0.41,0.565-0.719,0.719l-5.788,2.895C10.348,16.443,10.107,16.5,9.857,16.5z M9.856,9C9.798,9,9.75,9.047,9.75,9.106
                                            v5.788c0,0.016,0.004,0.033,0.011,0.047c0.013,0.027,0.034,0.044,0.061,0.054C9.834,14.998,9.845,15,9.856,15
                                            c0.016,0,0.032-0.004,0.047-0.011l5.788-2.895c0.02-0.01,0.038-0.027,0.047-0.047c0.026-0.052,0.005-0.115-0.047-0.141l-5.79-2.895
                                            C9.889,9.004,9.872,9,9.856,9z">
</path>
</g>
</svg>
</i>Car Video
</a>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<div class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item bravo_embed_video" src="" allowscriptaccess="always" allow="autoplay"></iframe>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="bravo_content">
<div class="container">
<div class="row">
<div class="col-md-12 col-lg-9">
<div class="g-header">
<div class="left">
<h2>{{$car->title}}</h2>
<p class="address"><i class="fa fa-map-marker"></i>
    {{$car->subtitle}}
</p>
</div>

</div>
<div class="g-gallery">
<div class="fotorama" data-width="100%" data-thumbwidth="135" data-thumbheight="135" data-thumbmargin="15" data-nav="thumbs" data-allowfullscreen="true">

@php($id=$car->id)
@php($carimage=DB::table('carimages')->where('car_id',$id)->get())
@foreach($carimage as $carimage)
<a href="/upload/{{$carimage->image}}" data-thumb="/upload/{{$carimage->image}}"></a>
@endforeach

</div>
<div class="social">
<div class="social-share">
<span class="social-icon">
<i class="icofont-share"></i>
</span>
<ul class="share-wrapper">
<li>
<a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=https://bookingcore.org/en/car/2019-audi-s3&title=2019 Audi S3" target="_blank" rel="noopener" original-title="Facebook">
<i class="fa fa-facebook fa-lg"></i>
</a>
</li>
<li>
<a class="twitter" href="https://twitter.com/share?url=https://bookingcore.org/en/car/2019-audi-s3&title=2019 Audi S3" target="_blank" rel="noopener" original-title="Twitter">
<i class="fa fa-twitter fa-lg"></i>
</a>
</li>
</ul>
</div>
<div class="service-wishlist " data-id="5" data-type="car">
<i class="fa fa-heart-o"></i>
</div>
</div>
</div>
<div class="g-overview">
<h3>Description</h3>
<div class="description">
<p>{{$car->description}}</p> </div>
</div>

</div>


<div class="col-md-12 col-lg-3">
<div class="owner-info widget-box">
<div class="media">
<div class="media-left">
<a href="en\profile\1.html" target="_blank">
<img class="avatar avatar-96 photo origin round" src="uploads\0000\1\2019\11\15\portfolio7-350x400-150.jpg" alt="System Admin">
</a>
</div>
<div class="media-body">
<h4 class="media-heading"><a class="author-link" href="en\profile\1.html" target="_blank">System Admin</a>
<img data-toggle="tooltip" data-placement="top" src="icon\ico-vefified-1.svg" title="Verified" alt="ico-vefified-1">
</h4>
<p>Member Since Nov 2019</p>
</div>
</div>
</div>
<div class="bravo_single_book_wrap">
<div class="bravo_single_book">
<div id="bravo_space_book_app" v-cloak="">
<div class="form-head">
<div class="price">
<span class="label">
from
</span>
<span class="value">
<span class="onsale"></span>
<span class="text-lg">$300</span>
</span>
</div>
</div>
<div class="form-content">
<div class="form-group form-date-field form-date-search clearfix " data-format="MM/DD/YYYY">
<div class="date-wrapper clearfix" @click="openStartDate">
<div class="check-in-wrapper">
<label>Select Dates</label>
<div class="render check-in-render" v-html="start_date_html"></div>
</div>
<i class="fa fa-angle-down arrow"></i>
</div>
<input type="text" class="start_date" ref="start_date" style="height: 1px; visibility: hidden">
</div>
<div class="form-group form-guest-search">
<div class="guest-wrapper d-flex justify-content-between align-items-center">
<div class="flex-grow-1">
<label>Number</label>
</div>
<div class="flex-shrink-0">
<div class="input-number-group">
<i class="icon ion-ios-remove-circle-outline" @click="minusNumberType()"></i>
<span class="input"></span>
<i class="icon ion-ios-add-circle-outline" @click="addNumberType()"></i>
</div>
</div>
</div>
</div>
<div class="form-section-group form-group" v-if="extra_price.length">
<h4 class="form-section-title">Extra prices:</h4>
<div class="form-group " v-for="(type,index) in extra_price">
<div class="extra-price-wrap d-flex justify-content-between">
<div class="flex-grow-1">
<label><input type="checkbox" v-model="type.enable"> </label>
<div class="render" v-if="type.price_type"></div>
</div>
<div class="flex-shrink-0">
</div>
</div>
</div>
</div>
<div class="form-section-group form-group-padding" v-if="buyer_fees.length">
<div class="extra-price-wrap d-flex justify-content-between" v-for="(type,index) in buyer_fees">
<div class="flex-grow-1">
<label>
<i class="icofont-info-circle" v-if="type.desc" data-toggle="tooltip" data-placement="top" :title="type.type_desc"></i>
</label>
<div class="render" v-if="type.price_type"></div>
</div>
<div class="flex-shrink-0">
</div>
</div>
</div>
</div>
<div class="form-section-total" v-if="total_price > 0">
<label>Total</label>
<span class="price"></span>
</div>
<div v-html="html"></div>
<div class="submit-group">
<p><i>
Guest in maximum
</i>
</p>
<a class="btn btn-large" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'btn-primary':step == 1}" name="submit">
<span v-if="step == 1">BOOK NOW</span>
<span v-if="step == 2">Book Now</span>
<i v-show="onSubmit" class="fa fa-spinner fa-spin"></i>
</a>
<div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row end_tour_sticky">
<div class="col-md-12">
</div>
</div>
</div>
</div>
<div class="bravo-more-book-mobile">
<div class="container">
<div class="left">
<div class="g-price">
<div class="prefix">
<span class="fr_text">from</span>
</div>
<div class="price">
<span class="onsale"></span>
<span class="text-price">$300</span>
</div>
</div>
<div class="service-review tour-review-4.3">
<div class="list-star">
<ul class="booking-item-rating-stars">
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
<li><i class="fa fa-star-o"></i></li>
</ul>
<div class="booking-item-rating-stars-active" style="width: 86%">
<ul class="booking-item-rating-stars">
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
<li><i class="fa fa-star"></i></li>
</ul>
</div>
</div>
<span class="review">
3 Reviews
</span>
</div>
</div>
<div class="right">
<a class="btn btn-primary bravo-button-book-mobile">Book Now</a>
</div>
</div>
</div> </div>
@endforeach
@endsection