﻿@extends('layouts.master')
@section('body')

<div class="page-template-content">
<div class="bravo-form-search-all" style="background-image: linear-gradient(0deg,rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url('uploads/demo/general/home-mix.jpg') !important">
<div class="container">
<div class="row">
<div class="col-lg-12">
<h1 class="text-heading">Hi There!</h1>
<div class="sub-heading">Where would you like to go?</div>
<div class="g-form-control">
<ul class="nav nav-tabs" role="tablist">
<!--<li role="bravo_hotel">
<a href="#bravo_hotel" class=" active " aria-controls="bravo_hotel" role="tab" data-toggle="tab">
 <i class="fa fa-building-o"></i>
Hotel
</a>
</li>
<li role="bravo_space">
<a href="#bravo_space" class="" aria-controls="bravo_space" role="tab" data-toggle="tab">
<i class="icofont-building-alt"></i>
Space
</a>
</li>-->


<li role="bravo_tour">
<a href="#bravo_tour" class="active" aria-controls="bravo_tour" role="tab" data-toggle="tab">
<i class="icofont-island-alt"></i>
Tour & Travel
</a>
</li>


<li role="bravo_car">
<a href="#bravo_car" class="" aria-controls="bravo_car" role="tab" data-toggle="tab">
<i class="icofont-car"></i>
Car service
</a>
</li>

<li role="bravo_mobile">
<a href="#bravo_mobile" class="" aria-controls="bravo_mobile" role="tab" data-toggle="tab">
<i class="icofont-stock-mobile"></i>
Mobile Accessories
</a>
</li>

<li role="bravo_ticket">
    <a href="#bravo_ticket" class="" aria-controls="bravo_ticket" role="tab" data-toggle="tab">
        <i class="icofont-ticket"></i>
    Ticket Enquiry
    </a>
    </li>
</ul>



<div class="tab-content">

<!--
<div role="tabpanel" class="tab-pane  active " id="bravo_hotel">
<form action="https://bookingcore.org/en/hotel" class="form bravo_form" method="get">
<div class="g-field-search">
<div class="row">
<div class="col-md-4 border-right">
<div class="form-group">
<i class="field-icon fa icofont-map"></i>
<div class="form-content">
<label>Location</label>
<div class="smart-search">
<input type="text" class="smart-search-location parent_text form-control" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
<input type="hidden" class="child_id" name="location_id" value="">
</div>
</div>
</div>
</div>
<div class="col-md-4 border-right">
<div class="form-group">
<i class="field-icon icofont-wall-clock"></i>
<div class="form-content">
<div class="form-date-search-hotel">
<div class="date-wrapper">
<div class="check-in-wrapper">
<label>Check In - Out</label>
<div class="render check-in-render">01/20/2020</div>
<span> - </span>
<div class="render check-out-render">01/21/2020</div>
</div>
</div>
<input type="hidden" class="check-in-input" value="01/20/2020" name="start">
<input type="hidden" class="check-out-input" value="01/21/2020" name="end">
<input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
</div>
</div>
</div>
</div>
<div class="col-md-4 border-right dropdown form-select-guests">
<div class="form-group">
<i class="field-icon icofont-travelling"></i>
<div class="form-content dropdown-toggle" data-toggle="dropdown">
<div class="wrapper-more">
<label>Guests</label>
<div class="render">
<span class="adults"><span class="one ">1 Adult</span> <span class=" d-none  multi" data-html=":count Adults">1 Adults</span></span>
-
<span class="children">
<span class="one " data-html=":count Child">0 Child</span>
<span class="multi  d-none " data-html=":count Children">0 Children</span>
</span>
</div>
</div>
</div>
<div class="dropdown-menu select-guests-dropdown">
<input type="hidden" name="adults" value="1" min="1" max="20">
<input type="hidden" name="children" value="0" min="0" max="20">
<input type="hidden" name="room" value="1" min="1" max="20">
<div class="dropdown-item-row">
<div class="label">Rooms</div>
<div class="val">
<span class="btn-minus" data-input="room"><i class="icon ion-md-remove"></i></span>
<span class="count-display">1</span>
<span class="btn-add" data-input="room"><i class="icon ion-ios-add"></i></span>
</div>
</div>
<div class="dropdown-item-row">
<div class="label">Adults</div>
<div class="val">
<span class="btn-minus" data-input="adults"><i class="icon ion-md-remove"></i></span>
<span class="count-display">1</span>
<span class="btn-add" data-input="adults"><i class="icon ion-ios-add"></i></span>
</div>
</div>
<div class="dropdown-item-row">
<div class="label">Children</div>
<div class="val">
<span class="btn-minus" data-input="children"><i class="icon ion-md-remove"></i></span>
<span class="count-display">0</span>
<span class="btn-add" data-input="children"><i class="icon ion-ios-add"></i></span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="g-button-submit">
<button class="btn btn-primary btn-search" type="submit">Search</button>
</div>
</form> </div>



<div role="tabpanel" class="tab-pane " id="bravo_space">
<form action="https://bookingcore.org/en/space" class="form bravo_form" method="get">
<div class="g-field-search">
<div class="row">
<div class="col-md-4 border-right">
<div class="form-group">
<i class="field-icon fa icofont-map"></i>
<div class="form-content">
<label>Location</label>
<div class="smart-search">
<input type="text" class="smart-search-location parent_text form-control" readonly="" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
<input type="hidden" class="child_id" name="location_id" value="">
</div>
</div>
</div>
</div>
<div class="col-md-4 border-right">
<div class="form-group">
<i class="field-icon icofont-wall-clock"></i>
<div class="form-content">
<div class="form-date-search">
<div class="date-wrapper">
<div class="check-in-wrapper">
<label>From - To</label>
<div class="render check-in-render">01/20/2020</div>
<span> - </span>
<div class="render check-out-render">01/21/2020</div>
</div>
</div>
<input type="hidden" class="check-in-input" value="01/20/2020" name="start">
<input type="hidden" class="check-out-input" value="01/21/2020" name="end">
<input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
</div>
</div>
</div>
</div>
<div class="col-md-4 border-right dropdown form-select-guests">
<div class="form-group">
<i class="field-icon icofont-travelling"></i>
<div class="form-content dropdown-toggle" data-toggle="dropdown">
<div class="wrapper-more">
<label>Guests</label>
<div class="render">
<span class="adults"><span class="one ">1 Adult</span> <span class=" d-none  multi" data-html=":count Adults">1 Adults</span></span>
-
<span class="children">
<span class="one " data-html=":count Child">0 Child</span>
<span class="multi  d-none " data-html=":count Children">0 Children</span>
</span>
</div>
</div>
</div>
<div class="dropdown-menu select-guests-dropdown">
<input type="hidden" name="adults" value="1" min="1" max="20">
<input type="hidden" name="children" value="0" min="0" max="20">
<div class="dropdown-item-row">
<div class="label">Adults</div>
<div class="val">
<span class="btn-minus" data-input="adults"><i class="icon ion-md-remove"></i></span>
<span class="count-display">1</span>
<span class="btn-add" data-input="adults"><i class="icon ion-ios-add"></i></span>
</div>
</div>
<div class="dropdown-item-row">
<div class="label">Children</div>
<div class="val">
<span class="btn-minus" data-input="children"><i class="icon ion-md-remove"></i></span>
<span class="count-display">0</span>
<span class="btn-add" data-input="children"><i class="icon ion-ios-add"></i></span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="g-button-submit">
<button class="btn btn-primary btn-search" type="submit">Search</button>
</div>
</form> </div>-->


 <div role="tabpanel" class="tab-pane active" id="bravo_tour">
<form action="https://bookingcore.org/en/tour" class="form bravo_form" method="get">
<div class="g-field-search">
<div class="row">
<div class="col-md-4 border-right">
<div class="form-group">
<i class="field-icon fa icofont-map"></i>
<div class="form-content">
<label>Location</label>
<div class="smart-search">
<input type="text" class="smart-search-location parent_text form-control" readonly="" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
<input type="hidden" class="child_id" name="location_id" value="">
</div>
</div>
</div>
</div>
<div class="col-md-4 border-right">
<div class="form-group">
<i class="field-icon icofont-wall-clock"></i>
<div class="form-content">
<div class="form-date-search">
<div class="date-wrapper">
<div class="check-in-wrapper">
<label>From - To</label>
<div class="render check-in-render">01/20/2020</div>
<span> - </span>
<div class="render check-out-render">01/21/2020</div>
</div>
</div>
<input type="hidden" class="check-in-input" value="01/20/2020" name="start">
<input type="hidden" class="check-out-input" value="01/21/2020" name="end">
<input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
</div>
</div>
</div>
</div>

<div class="col-md-4 border-right dropdown form-select-guests">
<div class="form-group">
<i class="field-icon icofont-travelling"></i>
<div class="form-content dropdown-toggle" data-toggle="dropdown">
<div class="wrapper-more">
<label>Guests</label>
<div class="render">
<span class="adults"><span class="one ">1 Adult</span> <span class=" d-none  multi" data-html=":count Adults">1 Adults</span></span>
-
<span class="children">
<span class="one " data-html=":count Child">0 Child</span>
<span class="multi  d-none " data-html=":count Children">0 Children</span>
</span>
</div>
</div>
</div>
<div class="dropdown-menu select-guests-dropdown">
<input type="hidden" name="adults" value="1" min="1" max="20">
<input type="hidden" name="children" value="0" min="0" max="20">
<input type="hidden" name="room" value="1" min="1" max="20">
<div class="dropdown-item-row">
<div class="label">Rooms</div>
<div class="val">
<span class="btn-minus" data-input="room"><i class="icon ion-md-remove"></i></span>
<span class="count-display">1</span>
<span class="btn-add" data-input="room"><i class="icon ion-ios-add"></i></span>
</div>
</div>
<div class="dropdown-item-row">
<div class="label">Adults</div>
<div class="val">
<span class="btn-minus" data-input="adults"><i class="icon ion-md-remove"></i></span>
<span class="count-display">1</span>
<span class="btn-add" data-input="adults"><i class="icon ion-ios-add"></i></span>
</div>
</div>
<div class="dropdown-item-row">
<div class="label">Children</div>
<div class="val">
<span class="btn-minus" data-input="children"><i class="icon ion-md-remove"></i></span>
<span class="count-display">0</span>
<span class="btn-add" data-input="children"><i class="icon ion-ios-add"></i></span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="g-button-submit">
<button class="btn btn-primary btn-search" type="submit">Search</button>
</div>
</form> </div>


<div role="tabpanel" class="tab-pane " id="bravo_car">
    <form action="https://bookingcore.org/en/tour" class="form bravo_form" method="get">
        <div class="g-field-search">
        <div class="row">
        <div class="col-md-4 border-right">
        <div class="form-group">
        <i class="field-icon fa icofont-map"></i>
        <div class="form-content">
        <label>Location</label>
        <div class="smart-search">
        <input type="text" class="smart-search-location parent_text form-control" readonly="" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
        <input type="hidden" class="child_id" name="location_id" value="">
        </div>
        </div>
        </div>
        </div>
        <div class="col-md-4 border-right">
        <div class="form-group">
        <i class="field-icon icofont-wall-clock"></i>
        <div class="form-content">
        <div class="form-date-search">
        <div class="date-wrapper">
        <div class="check-in-wrapper">
        <label>From - To</label>
        <div class="render check-in-render">01/20/2020</div>
        <span> - </span>
        <div class="render check-out-render">01/21/2020</div>
        </div>
        </div>
        <input type="hidden" class="check-in-input" value="01/20/2020" name="start">
        <input type="hidden" class="check-out-input" value="01/21/2020" name="end">
        <input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
        </div>
        </div>
        </div>
        </div>
        
        <div class="col-md-4 border-right dropdown form-select-guests">
        <div class="form-group">
        <i class="field-icon icofont-travelling"></i>
        <div class="form-content dropdown-toggle" data-toggle="dropdown">
        <div class="wrapper-more">
        <label>Guests</label>
        <div class="render">
        <span class="adults"><span class="one ">1 Adult</span> <span class=" d-none  multi" data-html=":count Adults">1 Adults</span></span>
        -
        <span class="children">
        <span class="one " data-html=":count Child">0 Child</span>
        <span class="multi  d-none " data-html=":count Children">0 Children</span>
        </span>
        </div>
        </div>
        </div>
        <div class="dropdown-menu select-guests-dropdown">
        <input type="hidden" name="adults" value="1" min="1" max="20">
        <input type="hidden" name="children" value="0" min="0" max="20">
        <input type="hidden" name="room" value="1" min="1" max="20">
        <div class="dropdown-item-row">
        <div class="label">Rooms</div>
        <div class="val">
        <span class="btn-minus" data-input="room"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">1</span>
        <span class="btn-add" data-input="room"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        <div class="dropdown-item-row">
        <div class="label">Adults</div>
        <div class="val">
        <span class="btn-minus" data-input="adults"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">1</span>
        <span class="btn-add" data-input="adults"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        <div class="dropdown-item-row">
        <div class="label">Children</div>
        <div class="val">
        <span class="btn-minus" data-input="children"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">0</span>
        <span class="btn-add" data-input="children"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-button-submit">
        <button class="btn btn-primary btn-search" type="submit">Search</button>
        </div>
        </form> 
</div>



<div role="tabpanel" class="tab-pane " id="bravo_mobile">
    <form action="https://bookingcore.org/en/tour" class="form bravo_form" method="get">
        <div class="g-field-search">
        <div class="row">
        <div class="col-md-4 border-right">
        <div class="form-group">
        <i class="field-icon fa icofont-map"></i>
        <div class="form-content">
        <label>Location</label>
        <div class="smart-search">
        <input type="text" class="smart-search-location parent_text form-control" readonly="" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
        <input type="hidden" class="child_id" name="location_id" value="">
        </div>
        </div>
        </div>
        </div>
        <div class="col-md-4 border-right">
        <div class="form-group">
        <i class="field-icon icofont-wall-clock"></i>
        <div class="form-content">
        <div class="form-date-search">
        <div class="date-wrapper">
        <div class="check-in-wrapper">
        <label>From - To</label>
        <div class="render check-in-render">01/20/2020</div>
        <span> - </span>
        <div class="render check-out-render">01/21/2020</div>
        </div>
        </div>
        <input type="hidden" class="check-in-input" value="01/20/2020" name="start">
        <input type="hidden" class="check-out-input" value="01/21/2020" name="end">
        <input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
        </div>
        </div>
        </div>
        </div>
        
        <div class="col-md-4 border-right dropdown form-select-guests">
        <div class="form-group">
        <i class="field-icon icofont-travelling"></i>
        <div class="form-content dropdown-toggle" data-toggle="dropdown">
        <div class="wrapper-more">
        <label>Guests</label>
        <div class="render">
        <span class="adults"><span class="one ">1 Adult</span> <span class=" d-none  multi" data-html=":count Adults">1 Adults</span></span>
        -
        <span class="children">
        <span class="one " data-html=":count Child">0 Child</span>
        <span class="multi  d-none " data-html=":count Children">0 Children</span>
        </span>
        </div>
        </div>
        </div>
        <div class="dropdown-menu select-guests-dropdown">
        <input type="hidden" name="adults" value="1" min="1" max="20">
        <input type="hidden" name="children" value="0" min="0" max="20">
        <input type="hidden" name="room" value="1" min="1" max="20">
        <div class="dropdown-item-row">
        <div class="label">Rooms</div>
        <div class="val">
        <span class="btn-minus" data-input="room"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">1</span>
        <span class="btn-add" data-input="room"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        <div class="dropdown-item-row">
        <div class="label">Adults</div>
        <div class="val">
        <span class="btn-minus" data-input="adults"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">1</span>
        <span class="btn-add" data-input="adults"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        <div class="dropdown-item-row">
        <div class="label">Children</div>
        <div class="val">
        <span class="btn-minus" data-input="children"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">0</span>
        <span class="btn-add" data-input="children"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-button-submit">
        <button class="btn btn-primary btn-search" type="submit">Search</button>
        </div>
        </form> 
</div>

<div role="tabpanel" class="tab-pane " id="bravo_ticket">
    <form action="https://bookingcore.org/en/tour" class="form bravo_form" method="get">
        <div class="g-field-search">
        <div class="row">
        <div class="col-md-4 border-right">
        <div class="form-group">
        <i class="field-icon fa icofont-map"></i>
        <div class="form-content">
        <label>Location</label>
        <div class="smart-search">
        <input type="text" class="smart-search-location parent_text form-control" readonly="" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
        <input type="hidden" class="child_id" name="location_id" value="">
        </div>
        </div>
        </div>
        </div>
        <div class="col-md-4 border-right">
        <div class="form-group">
        <i class="field-icon icofont-wall-clock"></i>
        <div class="form-content">
        <div class="form-date-search">
        <div class="date-wrapper">
        <div class="check-in-wrapper">
        <label>From - To</label>
        <div class="render check-in-render">01/20/2020</div>
        <span> - </span>
        <div class="render check-out-render">01/21/2020</div>
        </div>
        </div>
        <input type="hidden" class="check-in-input" value="01/20/2020" name="start">
        <input type="hidden" class="check-out-input" value="01/21/2020" name="end">
        <input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
        </div>
        </div>
        </div>
        </div>
        
        <div class="col-md-4 border-right dropdown form-select-guests">
        <div class="form-group">
        <i class="field-icon icofont-travelling"></i>
        <div class="form-content dropdown-toggle" data-toggle="dropdown">
        <div class="wrapper-more">
        <label>Guests</label>
        <div class="render">
        <span class="adults"><span class="one ">1 Adult</span> <span class=" d-none  multi" data-html=":count Adults">1 Adults</span></span>
        -
        <span class="children">
        <span class="one " data-html=":count Child">0 Child</span>
        <span class="multi  d-none " data-html=":count Children">0 Children</span>
        </span>
        </div>
        </div>
        </div>
        <div class="dropdown-menu select-guests-dropdown">
        <input type="hidden" name="adults" value="1" min="1" max="20">
        <input type="hidden" name="children" value="0" min="0" max="20">
        <input type="hidden" name="room" value="1" min="1" max="20">
        <div class="dropdown-item-row">
        <div class="label">Rooms</div>
        <div class="val">
        <span class="btn-minus" data-input="room"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">1</span>
        <span class="btn-add" data-input="room"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        <div class="dropdown-item-row">
        <div class="label">Adults</div>
        <div class="val">
        <span class="btn-minus" data-input="adults"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">1</span>
        <span class="btn-add" data-input="adults"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        <div class="dropdown-item-row">
        <div class="label">Children</div>
        <div class="val">
        <span class="btn-minus" data-input="children"><i class="icon ion-md-remove"></i></span>
        <span class="count-display">0</span>
        <span class="btn-add" data-input="children"><i class="icon ion-ios-add"></i></span>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-button-submit">
        <button class="btn btn-primary btn-search" type="submit">Search</button>
        </div>
        </form> 
</div>


</div>
</div>
</div>
</div>
</div>
</div>
<div class="bravo-offer">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="item">
<div class="featured-text">HOLIDAY SALE</div>
<h2 class="item-title">Special Offers</h2>
<p class="item-sub-title">Find Your Perfect Hotels Get the best<br>
prices on 20,000+ properties<br>
the best prices on</p>
<a href="#" class="btn btn-default">See Deals</a>
<div class="img-cover" style="background: url('uploads/demo/general/image_home_mix_1.jpg')"></div>
</div>
</div>
<div class="col-lg-3">
<div class="item">
<div class="featured-icon"><i class="icofont-email"></i></div>
<h2 class="item-title">Newsletters</h2>
<p class="item-sub-title">Join for free and get our <br>
tailored newsletters full of <br>
hot travel deals.</p>
<a href="register.html" class="btn btn-default">Sign Up</a>
<div class="img-cover" style="background: url('uploads/demo/general/image_home_mix_2.jpg')"></div>
</div>
</div>
<div class="col-lg-3">
<div class="item">
<div class="featured-icon"><i class="icofont-island-alt"></i></div>
<h2 class="item-title">Travel Tips</h2>
<p class="item-sub-title">Tips from our travel experts to <br>
make your next trip even<br>
better.</p>
<a href="register.html" class="btn btn-default">Sign Up</a>
<div class="img-cover" style="background: url('uploads/demo/general/image_home_mix_3.jpg')"></div>
</div>
</div>
</div>
</div>
</div>
<div class="container">

</div>









<div class="bravo-list-tour box_shadow">
<div class="container">
<div class="title">
Our best promotion tours
<div class="sub-title">

</div>
</div>
<div class="list-item">
<div class="row row-eq-height">

  @php($travel=DB::table('travels')->limit(6)->get())
  @foreach($travel as $travel)

<div class="col-lg-4 col-md-6 col-item">
<div class="item">
<div class="header-thumb">
    @php($id=$travel->id)
    @php($travelimage=DB::table('travelimages')->where('travel_id',$id)->limit(1)->get())
    @foreach($travelimage as $travelimage)
<img class='img-responsive lazy' data-src="upload/{{$travelimage->image}}" alt='American Parks Trail end Rapid City'>
    @endforeach

<a class="st-btn st-btn-primary tour-book-now" href="/tour-and-travel-details?id={{$travel->id}}">Book now</a>
<div class="service-wishlist " data-id="1" data-type="tour">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="caption">
<div class="title-address">
<h3 class="title"><a href="/tour-and-travel-details?id={{$travel->id}}"> {{$travel->title}} </a></h3>
<p class="duration">
<span class="text-price">
    Rs. {{$travel->price}}
</span>


</p>
</div>

</div>
</div> </div>
@endforeach



</div>
</div>
</div>
</div>

<!--<div class="container">
<div class="bravo-list-space layout_normal">
<div class="title">
Rental Listing
</div>
<div class="sub-title">
Homes highly rated for thoughtful design
</div>
<div class="list-item">
<div class="row">
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\stay-greenwich-village.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-11.jpg" alt='STAY GREENWICH VILLAGE'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale">$300</span>
<span class="text-price">$150 <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="11" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\stay-greenwich-village.html">
STAY GREENWICH VILLAGE
</a>
<div class="sale_info">50%</div>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.8/5 <span class="rate-text">Excellent</span>
</span>
<span class="review">
5 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 8
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 5
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 10
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\lily-dale-village.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-10.jpg" alt='LILY DALE VILLAGE'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale"></span>
<span class="text-price">$250 <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="10" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\lily-dale-village.html">
LILY DALE VILLAGE
</a>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.0/5 <span class="rate-text">Very Good</span>
</span>
<span class="review">
2 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 6
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 4
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 9
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 101 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="featured">
Featured
</div>
<div class="thumb-image ">
<a href="en\space\luxury-single.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-9.jpg" alt='LUXURY SINGLE'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale">$400</span>
<span class="text-price">$350 <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="9" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\luxury-single.html">
<i class="fa fa-bolt d-none"></i>
LUXURY SINGLE
</a>
<div class="sale_info">12%</div>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.5/5 <span class="rate-text">Excellent</span>
</span>
<span class="review">
2 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 6
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 9
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 8
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\space\paris-greenwich-villa.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/space/space-8.jpg" alt='PARIS GREENWICH VILLA'>
</a>
<div class="price-wrapper">
<div class="price">
<span class="onsale"></span>
<span class="text-price">$500 <span class="unit">/night</span></span>
</div>
</div>
<div class="service-wishlist " data-id="8" data-type="space">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="item-title">
<a href="en\space\paris-greenwich-villa.html">
<i class="fa fa-bolt d-none"></i>
PARIS GREENWICH VILLA
</a>
</div>
<div class="location">
Paris
</div>
<div class="service-review">
<span class="rate">
4.5/5 <span class="rate-text">Excellent</span>
</span>
<span class="review">
4 Reviews
</span>
</div>
<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="No. People">
<i class="input-icon field-icon icofont-people  "></i> 7
</span>
<span class="amenity bed" data-toggle="tooltip" title="No. Bed">
<i class="input-icon field-icon icofont-hotel"></i> 9
</span>
<span class="amenity bath" data-toggle="tooltip" title="No. Bathroom">
<i class="input-icon field-icon icofont-bathtub"></i> 1
</span>
<span class="amenity size" data-toggle="tooltip" title="Square">
<i class="input-icon field-icon icofont-ruler-compass-alt"></i> 127 sqft
</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>-->


<div class="container">
<div class="bravo-list-car layout_normal">
<div class="title">
Car Trending
</div>
<div class="sub-title">
Book incredible things to do around the world.
</div>
<div class="list-item">
<div class="row">

    @php($car=DB::table('cars')->limit(8)->get())
    @foreach($car as $car)
<div class="col-lg-3 col-md-6">
<div class="item-loop ">
<div class="thumb-image ">
<a href="en\car\vinfast-fadil-standard.html">

    @php($id=$car->id)
    @php($carimage=DB::table('carimages')->where('car_id',$id)->limit(1)->get())
    @foreach($carimage as $carimage)
<img class='img-responsive lazy' data-src="upload/{{$carimage->image}}" alt='Vinfast Fadil Standard'>
    @endforeach

</a>
<div class="service-wishlist " data-id="13" data-type="car">
<i class="fa fa-heart-o"></i>
</div>
</div>
<div class="item-title">
<a href="en\car\vinfast-fadil-standard.html">
<i class="fa fa-bolt d-none"></i>
{{$car->title}}
</a>
</div>

<div class="amenities clearfix">
<span class="amenity total" data-toggle="tooltip" title="Passenger">
<i class="input-icon field-icon icon-passenger  "></i>
<span class="text">
{{$car->guest}}
</span>
</span>

<span class="onsale"></span>
<span class="text-price">Rs. {{$car->price}}<span class="unit">/night</span></span>

</div>

</div>
</div>
@endforeach()




</div>
</div>
</div>
</div>
<div class="bravo-list-news">
<!--<div class="container">
<div class="title">
Read the latest from blog
<div class="sub-title">
Contrary to popular belief
</div>
</div>
<div class="list-item">
<div class="row">
<div class="col-lg-4 col-md-6">
<div class="item-news">
<div class="thumb-image">
<a href="en\news\the-day-on-paris.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/news/news-1.jpg" alt='The day on Paris'>
</a>
</div>
<div class="caption">
<div class="item-date">
<ul>
<li>
<a href="en\news\category\adventure-travel.html">
Adventure Travel
</a>
</li>
<li class="dot"> 01/20/2020 </li>
</ul>
</div>
<h3 class="item-title"><a href="en\news\the-day-on-paris.html"> The day on Paris </a></h3>
<div class="item-desc">
From the iconic to the unexpected, the city of San Francisco never ceases to...
</div>
<div class="item-more">
<a class="btn-readmore" href="en\news\the-day-on-paris.html">Read More</a>
</div>
</div>
</div> </div>
<div class="col-lg-4 col-md-6">
<div class="item-news">
<div class="thumb-image">
<a href="en\news\pure-luxe-in-punta-mita.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/news/news-2.jpg" alt='Pure Luxe in Punta Mita'>
</a>
</div>
<div class="caption">
<div class="item-date">
<ul>
<li>
<a href="en\news\category\ecotourism.html">
Ecotourism
</a>
</li>
<li class="dot"> 01/20/2020 </li>
 </ul>
</div>
<h3 class="item-title"><a href="en\news\pure-luxe-in-punta-mita.html"> Pure Luxe in Punta Mita </a></h3>
<div class="item-desc">
From the iconic to the unexpected, the city of San Francisco never ceases to...
</div>
<div class="item-more">
<a class="btn-readmore" href="en\news\pure-luxe-in-punta-mita.html">Read More</a>
</div>
</div>
</div> </div>
<div class="col-lg-4 col-md-6">
<div class="item-news">
<div class="thumb-image">
<a href="en\news\all-aboard-the-rocky-mountaineer.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/news/news-3.jpg" alt='All Aboard the Rocky Mountaineer'>
</a>
</div>
<div class="caption">
<div class="item-date">
<ul>
<li>
<a href="en\news\category\hosted-tour.html">
Hosted Tour
</a>
</li>
<li class="dot"> 01/20/2020 </li>
</ul>
</div>
<h3 class="item-title"><a href="en\news\all-aboard-the-rocky-mountaineer.html"> All Aboard the Rocky Mountaineer </a></h3>
<div class="item-desc">
From the iconic to the unexpected, the city of San Francisco never ceases to...
</div>
<div class="item-more">
<a class="btn-readmore" href="en\news\all-aboard-the-rocky-mountaineer.html">Read More</a>
</div>
</div>
</div> </div>
<div class="col-lg-4 col-md-6">
<div class="item-news">
<div class="thumb-image">
<a href="en\news\city-spotlight-philadelphia.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/news/news-4.jpg" alt='City Spotlight: Philadelphia'>
</a>
</div>
<div class="caption">
<div class="item-date">
<ul>
<li>
<a href="en\news\category\hosted-tour.html">
Hosted Tour
</a>
</li>
<li class="dot"> 01/20/2020 </li>
</ul>
</div>
<h3 class="item-title"><a href="en\news\city-spotlight-philadelphia.html"> City Spotlight: Philadelphia </a></h3>
<div class="item-desc">
From the iconic to the unexpected, the city of San Francisco never ceases to...
</div>
<div class="item-more">
<a class="btn-readmore" href="en\news\city-spotlight-philadelphia.html">Read More</a>
</div>
</div>
</div> </div>
<div class="col-lg-4 col-md-6">
<div class="item-news">
<div class="thumb-image">
<a href="en\news\tiptoe-through-the-tulips-of-washington.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/news/news-5.jpg" alt='Tiptoe through the Tulips of Washington'>
</a>
</div>
<div class="caption">
<div class="item-date">
<ul>
<li>
<a href="en\news\category\ecotourism.html">
Ecotourism
</a>
 </li>
<li class="dot"> 01/20/2020 </li>
</ul>
</div>
<h3 class="item-title"><a href="en\news\tiptoe-through-the-tulips-of-washington.html"> Tiptoe through the Tulips of Washington </a></h3>
<div class="item-desc">
From the iconic to the unexpected, the city of San Francisco never ceases to...
</div>
<div class="item-more">
<a class="btn-readmore" href="en\news\tiptoe-through-the-tulips-of-washington.html">Read More</a>
</div>
</div>
</div> </div>
<div class="col-lg-4 col-md-6">
<div class="item-news">
<div class="thumb-image">
<a href="en\news\a-seaside-reset-in-laguna-beach.html">
<img class='img-responsive lazy' data-src="https://bookingcore.org/uploads/demo/news/news-6.jpg" alt='A Seaside Reset in Laguna Beach'>
</a>
</div>
<div class="caption">
<div class="item-date">
<ul>
<li>
<a href="en\news\category\adventure-travel.html">
Adventure Travel
</a>
</li>
<li class="dot"> 01/20/2020 </li>
</ul>
</div>
<h3 class="item-title"><a href="en\news\a-seaside-reset-in-laguna-beach.html"> A Seaside Reset in Laguna Beach </a></h3>
<div class="item-desc">
From the iconic to the unexpected, the city of San Francisco never ceases to...
</div>
<div class="item-more">
<a class="btn-readmore" href="en\news\a-seaside-reset-in-laguna-beach.html">Read More</a>
</div>
</div>
</div> </div>
</div>
</div>
</div>-->
</div>

@endsection