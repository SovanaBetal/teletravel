﻿@extends('layouts.master')
@section('body')

<div class="bravo_search_tour">
<div class="bravo_banner" style="background-image: url(uploads/demo/tour/banner-search.jpg)">
<div class="container">
<h1>
Search for tour
</h1>
</div>
</div>
<div class="bravo_form_search">
<div class="container">
<div class="row">
<div class="col-lg-9 col-md-12">
<form action="https://bookingcore.org/en/tour" class="form bravo_form" method="get">
<div class="g-field-search">
<div class="row">
<div class="col-md-6 border-right">
<div class="form-group">
<i class="field-icon fa icofont-map"></i>
<div class="form-content">
<label>Location</label>
<div class="smart-search">
<input type="text" class="smart-search-location parent_text form-control" readonly="" placeholder="Where are you going?" value="" data-onload="Loading..." data-default="[{&quot;id&quot;:1,&quot;title&quot;:&quot; Paris&quot;},{&quot;id&quot;:2,&quot;title&quot;:&quot; New York, United States&quot;},{&quot;id&quot;:3,&quot;title&quot;:&quot; California&quot;},{&quot;id&quot;:4,&quot;title&quot;:&quot; United States&quot;},{&quot;id&quot;:5,&quot;title&quot;:&quot; Los Angeles&quot;},{&quot;id&quot;:6,&quot;title&quot;:&quot; New Jersey&quot;},{&quot;id&quot;:7,&quot;title&quot;:&quot; San Francisco&quot;},{&quot;id&quot;:8,&quot;title&quot;:&quot; Virginia&quot;}]">
<input type="hidden" class="child_id" name="location_id" value="">
</div>
</div>
</div>
</div>
<div class="col-md-6 border-right">
<div class="form-group">
<i class="field-icon icofont-wall-clock"></i>
<div class="form-content">
<div class="form-date-search">
<div class="date-wrapper">
<div class="check-in-wrapper">
<label>From - To</label>
<div class="render check-in-render">01/20/2020</div>
<span> - </span>
<div class="render check-out-render">01/21/2020</div>
</div>
</div>
<input type="hidden" class="check-in-input" value="01/20/2020" name="start">
<input type="hidden" class="check-out-input" value="01/21/2020" name="end">
<input type="text" class="check-in-out" name="date" value="2020-01-20 - 2020-01-21">
</div>
</div>
</div>
</div>
</div>
</div>
<div class="g-button-submit">
<button class="btn btn-primary btn-search" type="submit">Search</button>
</div>
</form> </div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<!--<div class="col-lg-3 col-md-12">
<div class="bravo_filter">
<form action="https://bookingcore.org/en/tour" class="bravo_form_filter">
<div class="filter-title">
FILTER BY
</div>
<div class="g-filter-item">
<div class="item-title">
<h4>Filter Price</h4>
<i class="fa fa-angle-up" aria-hidden="true"></i>
</div>
<div class="item-content">
<div class="bravo-filter-price">
<input type="hidden" class="filter-price irs-hidden-input" name="price_range" data-symbol=" ¥" data-min="13302" data-max="188418" data-from="13302" data-to="188418" readonly="" value="">
<button type="submit" class="btn btn-link btn-apply-price-range">APPLY</button>
</div>
</div>
</div>
<div class="g-filter-item">
<div class="item-title">
<h4>Review Score</h4>
<i class="fa fa-angle-up" aria-hidden="true"></i>
</div>
<div class="item-content">
<ul>
<li>
<div class="bravo-checkbox">
<label>
<input name="review_score[]" type="checkbox" value="5">
<span class="checkmark"></span>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input name="review_score[]" type="checkbox" value="4">
<span class="checkmark"></span>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input name="review_score[]" type="checkbox" value="3">
<span class="checkmark"></span>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input name="review_score[]" type="checkbox" value="2">
<span class="checkmark"></span>
<i class="fa fa-star"></i>
<i class="fa fa-star"></i>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input name="review_score[]" type="checkbox" value="1">
<span class="checkmark"></span>
<i class="fa fa-star"></i>
</label>
</div>
</li>
</ul>
</div>
</div>
<div class="g-filter-item">
<div class="item-title">
<h4>Tour Type</h4>
<i class="fa fa-angle-up" aria-hidden="true"></i>
</div>
<div class="item-content">
<ul>
<li>
<div class="bravo-checkbox">
<label>
<input name="cat_id[]" type="checkbox" value="1"> City trips
<span class="checkmark"></span>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input name="cat_id[]" type="checkbox" value="2"> Ecotourism
<span class="checkmark"></span>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input name="cat_id[]" type="checkbox" value="3"> Escorted tour
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input name="cat_id[]" type="checkbox" value="4"> Ligula
<span class="checkmark"></span>
</label>
</div>
</li>
</ul>
<button type="button" class="btn btn-link btn-more-item">More <i class="fa fa-caret-down"></i></button>
</div>
</div>
<div class="g-filter-item">
<div class="item-title">
<h4> Travel Styles </h4>
<i class="fa fa-angle-up" aria-hidden="true"></i>
</div>
<div class="item-content">
<ul>
<li>
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="1"> Cultural
<span class="checkmark"></span>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="2"> Nature & Adventure
<span class="checkmark"></span>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="3"> Marine
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
 <input type="checkbox" name="terms[]" value="4"> Independent
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="5"> Activities
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="6"> Festival & Events
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="7"> Special Interest
<span class="checkmark"></span>
</label>
</div>
</li>
</ul>
<button type="button" class="btn btn-link btn-more-item">More <i class="fa fa-caret-down"></i></button>
</div>
</div>
<div class="g-filter-item">
<div class="item-title">
<h4> Facilities </h4>
<i class="fa fa-angle-up" aria-hidden="true"></i>
</div>
<div class="item-content">
<ul>
<li>
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="8"> Wifi
<span class="checkmark"></span>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="9"> Gymnasium
<span class="checkmark"></span>
</label>
</div>
</li>
<li>
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="10"> Mountain Bike
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="11"> Satellite Office
 <span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="12"> Staff Lounge
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="13"> Golf Cages
<span class="checkmark"></span>
</label>
</div>
</li>
<li class="hide">
<div class="bravo-checkbox">
<label>
<input type="checkbox" name="terms[]" value="14"> Aerobics Room
<span class="checkmark"></span>
</label>
</div>
</li>
</ul>
<button type="button" class="btn btn-link btn-more-item">More <i class="fa fa-caret-down"></i></button>
</div>
</div>
</form>
</div>
</div>-->
<div class="col-lg-12 col-md-12">
<div class="bravo-list-item">
<div class="topbar-search">
<!--<div class="text">
16 tours found
</div>-->
<div class="control">
</div>
</div>
<div class="list-item">
<div class="row">
    @foreach($travel as $travel)
<div class="col-lg-4 col-md-6">
<div class="item-tour ">
<div class="thumb-image">
<!--<div class="sale_info">94%</div>-->
<a target="_blank" href="/tour-and-travel-details?id={{$travel->id}}">

    @php($id=$travel->id)
    @php($travelimage=DB::table('travelimages')->where('travel_id',$id)->limit(1)->get())
    @foreach($travelimage as $travelimage)
    
<img class='img-responsive lazy' data-src="upload/{{$travelimage->image}}" alt='River Cruise Tour on the Seine'>
    @endforeach
</a>
<div class="service-wishlist " data-id="16" data-type="tour">
<i class="fa fa-heart"></i>
</div>
</div>
<div class="location">
<i class="icofont-paper-plane"></i>
{{$travel->title}}
</div>
<div class="item-title">
<a target="_blank" href="en\tour\river-cruise-tour-on-the-seine.html">
    {{$travel->title}}
</a>
</div>
<div class="service-review tour-review-3.9">

</div>
<div class="info">

<div class="g-price">
<div class="prefix">
<i class="icofont-flash"></i>
<span class="fr_text">from</span>
</div>
<div class="price">
<!--<span class="onsale">228.979 ¥</span>-->
<span class="text-price">Rs. {{$travel->price}}</span>
</div>
</div>
</div>
</div>
</div>
    @endforeach



<!--<div class="bravo-pagination">
<ul class="pagination" role="navigation">
<li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Previous">
<span class="page-link" aria-hidden="true">&lsaquo;</span>
</li>
<li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
<li class="page-item"><a class="page-link" href="tour-3.html?page=2">2</a></li>
<li class="page-item">
<a class="page-link" href="tour-3.html?page=2" rel="next" aria-label="Next &raquo;">&rsaquo;</a>
</li>
</ul>
<span class="count-string">Showing 1 - 9 of 16 Tours</span>
</div>-->
</div>
</div>
</div> </div>
</div>
@endsection